import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';

const Benefit = (props) => {
	if(typeof props.data.title === 'undefined') return null;
	let items = props.data.items ? props.data.items : [];
	let title = props.data.title ? props.data.title : '';
	return (
		<section className="crop-section skew black brandColor">
			<div className="wrap crop-content">
					<h2 className="title">{title}</h2>
		  	<ListGroup variant="flush">
			  	{items.map((item, index) => 
		  			<ListGroup.Item key={index}>
		  			<div className="row">
		  				<div className="col-1">
		  					{index+1}
		  				</div>
		  				<div className="col-11">
			  				<h3>{item.title}</h3>
			  				<span>{item.description}</span>
			  			</div>
		  			</div>
		  			</ListGroup.Item>
						)}
		  	</ListGroup>
		</div>
		</section>
	);
}
export default Benefit;
