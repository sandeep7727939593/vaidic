import React from 'react';

const PageContent = (props) => {
	if(typeof props.pageContent.title === 'undefined') return null;
	return (
		<section className="pt-4 pb-4">
						<div className="container">
							<div className="row">
								<div className="col-12">
									<h1 className="title">{props.pageContent.title}</h1>
									<p>{props.pageContent.content}</p>
								</div>
							</div>
						</div>
					</section>
	);
}
export default PageContent;
