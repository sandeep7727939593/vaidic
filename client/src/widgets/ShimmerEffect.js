import React, { Component } from "react";
// import './../static/css/shimmer.css';
import Shimmer from "react-shimmer-effect";
import injectSheet from "react-jss";

const StyleSheet = {
  container: {
    // border: "0px solid rgba(255, 255, 255, 1)",
    boxShadow: "0px 0px 200px rgba(0, 0, 0, .1)",
    borderRadius: "4px",
    backgroundColor: "white",
    // display: "flex",
    padding: "16px",
    width: "100%",
  },
  circle: {
    height: "56px",
    width: "56px",
    borderRadius: "50%"
  },
  line1: {
    width: "100%",
    height: "150px",
    alignSelf: "center",
    // margin: "100px",
    borderRadius: "0px"
  },
  line2: {
    width: "100%",
    height: "20px",
    alignSelf: "center",
    // margin: "100px",
    borderRadius: "0px"
  },
  line3: {
    width: "90%",
    height: "20px",
    alignSelf: "center",
    // margin: "100px",
    borderRadius: "0px"
  },
  line4: {
    width: "80%",
    height: "20px",
    alignSelf: "center",
    // margin: "100px",
    borderRadius: "0px"
  },
  line5: {
    width: "70%",
    height: "20px",
    alignSelf: "center",
    // margin: "100px",
    borderRadius: "0px"
  },
  line6: {
    width: "100%",
    height: "200px",
    alignSelf: "center",
    // margin: "100px",
    borderRadius: "0px"
  },
  type : ''
};

class ShimmerEffect extends Component {
  render() {

    const { classes, type } = this.props;
    return (
      <div className={classes.container}>
        {type === 'page' ? <div> 
        <Shimmer>
            <div className={classes.line1} />
            <div className={classes.line2} />
            <div className={classes.line2} />
            <div className={classes.line3} />
            <div className={classes.line4} />
            <div className={classes.line5} />
        </Shimmer>
        <Shimmer>
            <div className={classes.line1} />
            <div className={classes.line2} />
            <div className={classes.line2} />
            <div className={classes.line3} />
            <div className={classes.line4} />
            <div className={classes.line5} />
        </Shimmer>
        <Shimmer>
            <div className={classes.line1} />
            <div className={classes.line2} />
            <div className={classes.line2} />
            <div className={classes.line3} />
            <div className={classes.line4} />
            <div className={classes.line5} />
        </Shimmer></div> : ''}

        {type === 'card' ? <div className="container"> 
        <Shimmer>
            <div className={classes.line1} />
            <div className={classes.line2} />
            <div className={classes.line2} />
            <div className={classes.line3} />
            <div className={classes.line4} />
            <div className={classes.line5} />
        </Shimmer>
        <br />
        <div className="row">
          <div className="col-6">
            <Shimmer>
                <div className={classes.line6} />
                <div className={classes.line2} />
                <div className={classes.line2} />
                <div className={classes.line3} />
                <div className={classes.line4} />
                <div className={classes.line5} />
            </Shimmer>    
          </div>
          <div className="col-6">
            <Shimmer>
                <div className={classes.line6} />
                <div className={classes.line2} />
                <div className={classes.line2} />
                <div className={classes.line3} />
                <div className={classes.line4} />
                <div className={classes.line5} />
            </Shimmer>    
          </div>
        </div></div> : ''}
      </div>
    );
  }
}
 
export default injectSheet(StyleSheet) (ShimmerEffect);