/**
 * NewsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
	newsPage : async (req, res) => {
				// var d = await Product.find();
		let pageSize = req.query.pageSize ? req.query.pageSize : 5;
		let pageNo = req.query.pageNo ? req.query.pageNo : 1;

		let params = {
			fields : [
				'news.*',
				'author.name as author_name'
			],
			filters : [
				'status=1',
			],
			join : [
				'LEFT JOIN author ON news.id_author=author.id_author'
			]
		};
		var newsCount = await News.getList(params);
		params.limit = pageSize;
		params.page = pageNo;
		var news = await News.getList(params);
		var response = {};
		if(!sails.hooks.common.isEmptyObject(news.rows)) {
			news = news.rows;
			newsCount = newsCount.rows.length;
			let newsList = [];
			news.map((item, index) => {
				var obj = {
					title : item.title,
					content : sails.hooks.common.getShortDesc(item.content) + '...',
					img : sails.hooks.common.imageUrl(item.image, 'news-image/'),
					viewCount:item.view_count,
					url : sails.hooks.common.newsDetailUrl(item.link_rewrite, false),
					date : sails.hooks.common.dateFormatter(item.add_date),
					author_name : item.author_name,
				};
				newsList.push(obj);
			});
			response.newsList = newsList;
			response.url = sails.hooks.common.newsLandingUrl(false);
			response.pagination = {
				totalCount : newsCount,
				pageNo : pageNo,
				pageSize : pageSize,
				pageCount : Math.ceil(newsCount/pageSize)
			};
			response.pageContent = {
				title : 'News List',
				content : 'News marketing strategy is not what it used to be a few years ago. Today, it has gone to become a lot more serious, and much more effective, especially for businesses that have already managed to build an audience.'
			};
			response.seoData = {
				title : 'News Page',
				description : 'News Page @' + sails.config.domainName,
				index : '',
			};	
		}
		res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		res.json(response);
	},

	newsDetailPage : async (req, res) => {
		let slug = req.query.slug;
		let params = {
			fields : [
				'news.*',
				'author.name as author_name'
			],
			filters : [
				'status=1',
				`news.link_rewrite='${slug}'`
			],
			join : [
				'LEFT JOIN author ON news.id_author=author.id_author'
			]
		};
		var news = await News.getList(params);
		var response = {};
		if(!sails.hooks.common.isEmptyObject(news.rows)) {
			news = news.rows[0];
			news.addDate = sails.hooks.common.dateFormatter(news.add_date);
			news.img = sails.hooks.common.imageUrl(news.image, 'news-image/'),
			news.view_count = news.view_count+1;
			response = {
				breadcrumb : [
					{
			            text : 'Home',
			            title : 'Home',
			            url : '/',
			            active : false,
			        },
			        {
			            text : 'News',
			            title : 'News',
			            url : sails.hooks.common.newsLandingUrl(true),
			            active : false,
			        },
			        {
			            text : news.title,
			            title : news.title,
			            url : '',
			            active : true,
			        },
				],
				news : news,
				pageContent : {
					title : news.title,
					content : news.highlight
				},
				seoData : {
					title : news.title,
					description : news.highlight,
					index : '',
				}	
			};
			News.updateReord({id : news.id_news}, {view_count : parseInt(news.view_count)});
		}
		res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		res.json(response);
	}

};

