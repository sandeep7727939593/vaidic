/**
 * HomeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */



module.exports = {
  
	getProductPage: async (req, res) => {

		res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		
		let productslug = req.query.productslug;	
		let params = {
			filters : [
				`link_rewrite='${productslug}'`,
			]
		};
		var product = await Product.getList(params);
		if(sails.hooks.common.isEmptyObject(product.rows) || sails.hooks.common.isEmptyObject(product.rows[0])) {
			res.json(sails.hooks.common.redirectUrl('/'));
			return;
		}
		product = product.rows[0];

		let p = {
			filters : [
				'status=1',
				`id_product='${product.id_product}'`
			]
		};
		var model = await Model.getList(p);
		let items = [];
		if(sails.hooks.common.isEmptyObject(model.rows)) {
			res.json({});
		}
		model = model.rows;
		model.map((m, i) => {
			let q = {
	            title : m.model_name,
	            url : sails.hooks.common.modelUrl(product.link_rewrite, m.link_rewrite),
	            writeReviewUrl : sails.hooks.common.writeReviewUrl(m.id_model),
	            img : sails.hooks.common.imageUrl(m.cover_image, 'model/'),
	            desc : sails.hooks.common.getDesc(m.description),
	            ctaText : 'ADD TO CARD',
	            price : m.base_price,
	            rating : 2.5,
	        };
	        items.push(q);
		});

		let paramsFaq = {
			filters : [
				'status=1',
				`id_product='${product.id_product}'`,
				'id_model IS NULL',
			],
			orderBy : "id_faq desc",
			limit : 5,
		};
		var faq = await Faq.getList(paramsFaq);
		var faq = faq.rows;
		let faqList = [];
		faq.map((item, index) => {
			faqList.push({
				question : item.question,
                answer : item.answer,
			});
		});

		// GET Specification

		let paramsBenefit = {
			filters : [
				`id_product='${product.id_product}'`,
				'id_model IS NULL or id_model=0',
				`status=1`,
			]
		};
		var benefit = await Benefit.getList(paramsBenefit);
		benefit = benefit.rows;
		var benefitList = {};
		if(!sails.hooks.common.isEmptyObject(benefit[0])) {
			benefit = benefit[0];
			benefitList.items = JSON.parse(benefit['value'])
			benefitList.title = 'Benefit';
		}

		let data = {
			breadcrumb : [
				{
		            text : 'Home',
		            title : 'Home',
		            url : '/',
		            active : false,
		        },
		        {
		            text : product.title,
		            title : product.title,
		            url : sails.hooks.common.productUrl(product.link_rewrite),
		            active : true,
		        },
			],
			pageContent : {
		        title : product.title,
		        content : sails.hooks.common.getDesc(product.description),
		    },
			items : items,
			faqList : {
				title : 'Frequently Asked Questions',
				items : faqList,
			},
			benefitList : benefitList,
			seoData : {
				title : product.title,
				description : product.title + ' @' + sails.config.domainName,
				index : '',
			},
		};
		res.json(data);
	}
};

