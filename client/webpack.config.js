const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const configs = require('./src/configs/configs');
const PORT = process.env.PORT || 3000;

module.exports = {
  entry: './src/index.js',
  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: 'bundle.[hash].js',
  },
  devServer: {
    historyApiFallback: true,
    port: PORT,
    contentBase : './src/static'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      { 
        test: /\.css$/,
        use: [ 
          'style-loader',
          'css-loader', 
          'less-loader',
        ],
      },
      {
        test: /\.(png|jpe?g|gif|ttf|woff|svg|eot|ico)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      }
    ]
  },
  externals : {
    configs : JSON.stringify(configs),
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
      favicon: './public/favicon.ico'
    })
  ],
  resolve: {
      extensions: [".js", ".jsx"]
  }
};