import React from 'react';

const Cta = (props) => {
	const{btncls} = props;

	let handleClick = () => {
		let url = window.location.href;
		window.location.replace(url.includes("#leadFrom") ? "" : "#leadFrom");
	}
	return (	
		<button className={`btn ${btncls}`} type="button" onClick={handleClick} >{props.cta ? props.cta : 'Give Order'}</button>
	);
}
export default Cta;
