import React from 'react';
import Api from './../utils/Api';
import MetaTag from './../utils/MetaTag';
import NewsCard from './../widgets/NewsCard';
import PageContent from './../widgets/PageContent';
import globals from './../utils/globals';
import Pagination from './../widgets/Pagination';
import queryString from 'query-string';
import ShimmerEffect from './../widgets/ShimmerEffect';

const defaultProps = {
    breadcrumb : [],
    modelData : {
        title: ''
    },
    review : {},
    seoData : {},
    colcls: '',
    pagination : {},
    url : '',
    isLoad : false
};
class NewsPage extends React.Component {
	//Client Side
    constructor(props) {
        super(props);
        this.state = window.__INITIAL_STATE__ ||  defaultProps;
    }

    componentDidMount() {
        let self = this;
        self.setState(defaultProps);
        let params = queryString.parse(this.props.location.search)
        let q = {
            pageSize : 2,
            pageNo : params.pageno ? params.pageno : 1,
        };
        Api.getData('newsPage', q).then((res => {
            if(!globals.isEmptyObject(res)) {
                let o = {
                    pageContent : res.data && res.data.pageContent ? res.data.pageContent : '',
                    newsList : res.data && res.data.newsList ? res.data.newsList : [],
                    seoData : res.data && res.data.seoData ? res.data.seoData : {},
                    pagination : res.data && res.data.pagination ? res.data.pagination : {},
                    url : res.data && res.data.url ? res.data.url : defaultProps.url,
                    isLoad : true
                };
                self.setState(o);
            }
        })).catch((err) => {
            console.log(err);
        });
    }

    render() {
        if(!this.state.isLoad) {
            return (
                <main>
                    <ShimmerEffect type="card"/>
                </main>
            )
        }
        if(!this.state.newsList) {
            return null;
        }
        let cards = (items) => {
            return(
                <>
                    {items.map((item, index) => <NewsCard key={index} items={item} colcls={'col-lg-4 col-md-6'} />)}
                </>
            );
        }
        return (
            <main>
                <MetaTag data={this.state.seoData} />
                <PageContent pageContent={this.state.pageContent} />
                {this.state.newsList && <section className="pb-4">
                    <div className="container">
                        <div className="row">
                        {cards(this.state.newsList)}
                        </div>
                        <Pagination url={this.state.url} data={this.state.pagination}/>
                    </div>
                </section>}
		    </main>
		);
    }
}
NewsPage.defaultProps = defaultProps;
export default NewsPage;
