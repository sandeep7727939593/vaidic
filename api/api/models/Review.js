/**
 * Home.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
module.exports = {
    tableName : 'review',
    attributes: {
        id: { 
           type: 'number',  
           columnName : 'id_review',
           // required: true,
           unique:true,
           autoIncrement:true,
        },
        id_model: { 
            type: 'number', 
            required: true 
        },
        rating: { 
            type: 'number', 
            required: true 
        },
        review: { 
            type: 'string', 
            required: true 
        },
        name: { 
            type: 'string', 
            required: true 
        },
        phone_number: { 
            type: 'string'
        },
        add_date: { 
            type: 'ref', 
            required: true 
        },
        update_date: { 
            type: 'ref', 
            required: true 
        },
        status: { 
            type: 'number', 
            required: true 
        },
        quality: { 
            type: 'number', 
            required: true 
        },
    },
    getList : async (params = []) => {
        let query = Review.selectQuery('review', params);
        return await Review.getDatastore().sendNativeQuery(query, []);
    }
};

