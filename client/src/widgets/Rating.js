import ReactStars from "react-rating-stars-component";
import React from "react";
 
const Rating = (props) => {
    let ratingChanged = (url) => {
        console.log('on changerating ', url);
    }

    return(
        <a  href={props.url}>
          <ReactStars
            count={5}
            onChange={ratingChanged(props.url)}
           
            size={24}
            isHalf={true}
            emptyIcon={<i className="far fa-star"></i>}
            halfIcon={<i className="fa fa-star-half-alt"></i>}
            fullIcon={<i className="fa fa-star"></i>}
            activeColor="#ffd700"
            value={props.rating}
          />
      </a>
    );

};

export default Rating;