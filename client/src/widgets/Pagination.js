import React from 'react';
import Pagination from 'react-bootstrap/Pagination';


const PaginationBasic = (props) => {
	let data = props.data;
	if(typeof data === undefined) {
		return;
	}

	let active = parseInt(data.pageNo);
	let url = props.url ? props.url : '';
	let handleClick = (page) => {
		if(active !== page) {
			window.location.href = url + '?pageno=' + page;
		}
	}

	let items = [];
	for (let number = 1; number <= data.pageCount; number++) {
	  items.push(
	    <Pagination.Item key={number} active={number === active} onClick={(e) => handleClick(number)}>
	      {number}
	    </Pagination.Item>,
	  );
	}

	return (
		  <div>
		    <Pagination>{items}</Pagination>
		  </div>);
};

export default PaginationBasic;
