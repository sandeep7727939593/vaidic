import React from 'react';
import GoogleMapWidget from './../widgets/GoogleMapWidget';
import MetaTag from './../utils/MetaTag';
import configs from '../configs/configs';

const seoData = {
	title : configs.productName + ' Contact Us',
	description : 'Contact Us @' + configs.productName,
	index : '',
};

class ContactUsPage extends React.Component {
    render() {
    	return (
		    <main className="main clearfix">
            <div className="container">
				<section className="section clearfix">
					<h1 className="title">Contact Us</h1>
					<div className="row d-flex">
						<div className="col-4">
							<h3>Address</h3>
								Hp Hashala Group <br />
								Theen Dukhan, Badhal,<br />
								Jaipur, Rajstahn, India<br />
								Pin Code :- 303602<br />
						</div>
						<div className="col-4">
							<h3>Phone</h3>
								+91-9950123456<br />
								+91-9950123456<br />
								+91-9950123456<br />
						</div>
						<div className="col-4">
							<h3>Social Links</h3>
								Email :- sandeep7727939593@gmail.com<br />
								Facebook :- https://www.facebook.com/hpvedic<br />
								Instagram :- https://www.instagram.com/jangir6547/?hl=en<br />
						</div>
					</div>
					<h2> Our Google Location </h2>
					<GoogleMapWidget 
						isMarkerShown
						  googleMapURL="https://maps.googleapis.com/maps/api/js?v=16.exp&libraries=geometry,drawing,places"
						  loadingElement={<div style={{ height: `100%` }} />}
						  containerElement={<div style={{ height: `600px` }} />}
						  mapElement={<div style={{ height: `100%` }} />}
					/>
					<MetaTag data={seoData} />
				</section>

			</div>
		    </main>
		);
    }
}
export default ContactUsPage;
