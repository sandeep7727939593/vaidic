import React from 'react';
import Carousel from 'react-bootstrap/Carousel'

const defaultProps = {
  slidesToShow : 4,
  dots:true,
  showSides:true,
  sidesOpacity:.5,
  sideSize:.1,
  slidesToScroll:4,
  scrollOnDevice:true,
};

class Carousel2 extends React.Component {
  
    render() {
      if(!this.props.children) return null;
      return(
        <section className="section">
          <Carousel>
                {this.props.children}
            </Carousel>
        </section>);
    }

}
Carousel2.defaultProps = defaultProps;
export default Carousel2;
