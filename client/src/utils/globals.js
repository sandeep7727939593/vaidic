
const globals = {
	isEmptyObject : (obj) => {
	    try {
	        if (typeof obj === undefined  || obj == null || Object.keys(obj).length === 0) {
	            if(typeof obj === Array && obj.length > 0){
	                return false;
	            }
	        return true;
	        }
	        return false;    
	    } catch (e) {
	        return true;
	    }
	    
	},
};
export default globals;