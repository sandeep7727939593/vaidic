import React from 'react';
import Api from './../utils/Api';
import MetaTag from './../utils/MetaTag';
import VideoCard from './../widgets/VideoCard';
import PageContent from './../widgets/PageContent';
import globals from './../utils/globals';
import Pagination from './../widgets/Pagination';
import queryString from 'query-string';
import ShimmerEffect from './../widgets/ShimmerEffect';

const defaultProps = {
    breadcrumb : [],
    modelData : {
        title: ''
    },
    review : {},
    seoData : {},
    colcls:'',
    pagination : {},
    url : '',
    isLoad : false,
};
class VideoPage extends React.Component {
	//Client Side
    constructor(props) {
        super(props);
        this.state = window.__INITIAL_STATE__ ||  defaultProps;
    }

    componentDidMount() {
        let self = this;
        self.setState(defaultProps);
        let params = queryString.parse(this.props.location.search)
        let q = {
            pageSize : 1,
            pageNo : params.pageno ? params.pageno : 1,
        };
        Api.getData('videoPage', q).then((res => {
            if(!globals.isEmptyObject(res)) {
                let o = {
                    pageContent : res.data && res.data.pageContent ? res.data.pageContent : '',
                    videoList : res.data && res.data.videoList ? res.data.videoList : [],
                    seoData : res.data && res.data.seoData ? res.data.seoData : {},
                    pagination : res.data && res.data.pagination ? res.data.pagination : {},
                    url : res.data && res.data.url ? res.data.url : defaultProps.url,
                    isLoad : true
                };
                self.setState(o);
            }
        })).catch((err) => {
            console.log(err);
        });
    }

    render() {
        if(!this.state.isLoad) {
            return (
                <main className="home">
                    <ShimmerEffect type="card"/>
                </main>
            )
        }
        if(!this.state.videoList) {
            return null;
        }

        return (
		    <main className="video-page">
                <MetaTag  data={this.state.seoData}/>
                <PageContent pageContent={this.state.pageContent}/>
                <section className="pb-4">
                    <div className="container">
                        <div className="row">
                            <VideoCard items={this.state.videoList} colcls="col-lg-6" />
                        </div>
                        <Pagination url={this.state.url} data={this.state.pagination}/>
                    </div>
                </section>
		    </main>
		);
    }
}
VideoPage.defaultProps = defaultProps;
export default VideoPage;
