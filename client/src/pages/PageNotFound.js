import React from 'react';
import './../static/css/pageNotFound.css';

class PageNotFound extends React.Component {
    render() {
    	return (
		    <div className="pageNotFound">
                    Page Not Found 404
		    </div>
		);
    }
}
export default PageNotFound;
