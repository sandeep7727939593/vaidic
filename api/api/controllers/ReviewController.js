/**
 * HomeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */



module.exports = {
  
	postWriteReview: async (req, res) => {

		let params = req.body;
		let today = new Date();
		let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate() + ' ' + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		let o = {
			name:params.name,
			phone_number:params.phone_number,
			rating:parseInt(params.rating),
			review:params.review,
			id_model:params.id_model,
			quality:5,
			status: 1,
			add_date:date,
			update_date:date,
		}
		var record = await Review.create(o).fetch();
		let data = {
			redirectUrl : '/',
			code : 301,
		};
		res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  		res.header("Access-Control-Allow-Headers", "*");
		res.header( "Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, PATCH, OPTIONS" ); 
		res.json(data);
	}
};

