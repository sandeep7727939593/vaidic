import React from 'react';


const Validation = {
	phoneNumberValid : function(phoneNumber){
		phoneNumber = phoneNumber.replace(/[^\d\+]/g,"");
		console.log(phoneNumber)
		let error = '';
		let status = true;
		let regx = /^\d{10}$/;

		if(typeof phoneNumber === 'undefined' || phoneNumber == '') {
			status = false;
			error = 'Please Enter Phone Number';
		} else if(!phoneNumber.match(regx)) {
			status = false;
			error = 'Please Enter Valid Phone Number';
		}
		return {
			error : error,
			status : status
		};
	}
}

export default Validation;