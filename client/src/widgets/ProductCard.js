import React from 'react';
import Rating from './Rating';
import Cta from './Cta';
import {Card, CardDeck } from 'react-bootstrap'
import './../static/scss/_card.scss';

const ProductCard = (props) => {
	if(typeof props.item === 'undefined') return null;
  let item = props.item;
  const { colcls, btncls, handleClick} = props;
	
  return (
    <div className={colcls}>
      <CardDeck>
        <Card className="shadow">
          <div className="img-block bg-overlay-gradient-04" data-autofit="true" style={{ backgroundImage: `url(${item.logo})`, height: "250px" }}><Card.Img variant="top" src={item.img} alt={item.title} /></div>
          <Card.Body>
            <Card.Title><a href={item.url}>
              <h3 className="card-title">{item.title}</h3>
            </a></Card.Title>
            <h4 className="card-price">
              Price Rs. {item.price}
            </h4>
            <Rating name="half-rating" defaultValue={2.5} precision={0.5} rating={item.rating} url={item.writeReviewUrl} />
            <Cta cta={item.ctaText} btncls={btncls} handleClick={handleClick}/>
          </Card.Body>
        </Card>
      </CardDeck>
    </div>);

}

export default ProductCard;
