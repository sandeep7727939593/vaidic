 /*eslint no-unused-expressions: 0*/

import React,{ Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
const IndexPage = lazy(() => import('./pages/IndexPage'));
const ProductPage = lazy(() => import('./pages/ProductPage'));
const ModelPage = lazy(() => import('./pages/ModelPage'));
const WriteReviewPage = lazy(() => import('./pages/WriteReviewPage'));
const PageNotFound = lazy(() => import('./pages/PageNotFound'));
const VideoPage = lazy(() => import('./pages/VideoPage'));
const VideoDetailPage = lazy(() => import('./pages/VideoDetailPage'));
const NewsPage = lazy(() => import('./pages/NewsPage'));
const NewsDetailPage = lazy(() => import('./pages/NewsDetailPage'));
const ContactUsPage = lazy(() => import('./pages/ContactUsPage'));
const AboutUsPage = lazy(() => import('./pages/AboutUsPage'));
const EmbeddedPage = lazy(() => import('./pages/EmbeddedPage'));
const Footer = lazy(() => import('./layouts/Footer'));
const LeadFrom = lazy(() => import('./widgets/LeadFrom'));


class Routes extends React.Component {
  render() {
  	return(<Router>
			<Suspense fallback={<div>Loading...</div>}>
			  <Switch>
			    <Route path="*#leadFrom" component={PageNotFound}/>
			    <Route path="/:product.html" component={ProductPage}/>
			    <Route path="/write-review/:model" component={WriteReviewPage}/>
			    <Route path="/:product/:model.html" component={ModelPage}/>
			    <Route path="/videos/:videoUrl" component={VideoDetailPage}/>
			    <Route path="/videos" component={VideoPage}/>
			    <Route path="/news/:newsUrl" component={NewsDetailPage}/>
			    <Route path="/news" component={NewsPage}/>
			    <Route path="/contact-us" component={ContactUsPage}/>
			    <Route path="/about-us" component={AboutUsPage}/>
			    <Route path="/community" component={EmbeddedPage}/>
			    <Route exact path="/" component={IndexPage}/>
			    <Route path="*" component={PageNotFound}/>
			  </Switch>
   			<LeadFrom />
   			<Footer />
		</Suspense>
		</Router>); 
  	} 
}


export default Routes;