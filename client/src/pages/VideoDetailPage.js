import React from 'react';
import Api from './../utils/Api';
import MetaTag from './../utils/MetaTag';
import PageContent from './../widgets/PageContent';
import ReactPlayer from 'react-player/lazy'
import Breadcrumb from './../widgets/Breadcrumb';
import globals from './../utils/globals';
import ShimmerEffect from './../widgets/ShimmerEffect';

const defaultProps = {
    breadcrumb : [],
    modelData : {
        title: ''
    },
    review : {},
    seoData : {},
    isLoad : true
};
class VideoDetailPage extends React.Component {
	//Client Side
    constructor(props) {
        super(props);
        this.state = window.__INITIAL_STATE__ ||  defaultProps;
    }

    componentDidMount() {
        let self = this;
        self.setState(defaultProps);
        let params = this.props.match.params;
        let q = {
            slug : params.videoUrl,
        };
        Api.getData('videoDetailPage', q).then((res => {
            if(!globals.isEmptyObject(res)) {
                let o = {
                    videos : res.data.videos ? res.data.videos : {},
                    pageContent : res.data.pageContent ? res.data.pageContent : {},
                    seoData : res.data.seoData ? res.data.seoData : {},
                    breadcrumb : res.data.breadcrumb ? res.data.breadcrumb : [],
                    isLoad : true
                };
                self.setState(o);
            }
        })).catch((err) => {
            console.log(err);
        });
    }

    render() {
        if(!this.state.isLoad) {
            return (
                <main className="home">
                    <ShimmerEffect type="page"/>
                </main>
            )
        }
        if(!this.state.videos) {
            return null;
        }

        return (
		    <main className="video-detail-page">
                <MetaTag  data={this.state.seoData}/>
                <Breadcrumb breadcrumb={this.state.breadcrumb}/>
                <PageContent pageContent={this.state.pageContent}/>
                <section className="section">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <ReactPlayer url={this.state.videos.videoUrl} controls="true" volume="1" />
                            </div>
                        </div>
                    </div>
                </section>
                
		    </main>
		);
    }
}
VideoDetailPage.defaultProps = defaultProps;
export default VideoDetailPage;
