import React from 'react';

const Breadcrumb = (props) => {
	//Client Side
   	if(!props.breadcrumb) return null;
	let li = () => {
		let breadcrumb = props.breadcrumb;
		if(!breadcrumb) return null;
		let h = [];
		breadcrumb.map((item, index) => 
			h.push(<li key={index} className={`breadcrumb-item ${item.active ? 'active' : ''}`}>{item.active ? item.text : <a href={item.url} title={item.title}>{item.text}</a>}</li>)
		); 
		return h;
	}
	return (
		<div aria-label="breadcrumb" className="bg-light">
		 <div className="container">
						<div className="row">
								<div className="col-12">
									<ol className="breadcrumb mb-0">
										{li()}
									</ol>
								</div>
						</div>
			</div>
		</div>
	);
}
export default Breadcrumb;
