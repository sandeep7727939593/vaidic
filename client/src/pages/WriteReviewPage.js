import React from 'react';
import './../static/css/writeReviewPage.css';
import Api from './../utils/Api';
import ReactStars from "react-rating-stars-component";
import $ from 'jquery';

const defaultProps = {
    title : 'Haldi Review',
    rating : 0,
    phoneNumber : '',
    name : '',
    review : '',
    isPopup : false,
};


class WriteReviewPage extends React.Component {
	//Client Side
    constructor(props) {
        super(props);
        this.state = defaultProps;
        this.handleFrom = this.handleFrom.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        let o = JSON.parse(localStorage.getItem('writeReview'));
        this.setState(o);
    }

    phoneNumberValid (e){
        let phoneNumber = e.target.value;
        let error = '';
        let status = true;
        let regx = /^\d{10}$/;
        if(phoneNumber.length > 10) {
            phoneNumber = this.state.phoneNumber;
            $('.phoneNumber').val(phoneNumber);
        }
        if(typeof phoneNumber === 'undefined' || phoneNumber === '') {
            status = false;
            error = 'Please Enter Phone Number';
        } else if(!phoneNumber.match(regx)) {
            status = false;
            error = 'Please Enter Valid Phone Number';
        }

        this.setState({phoneNumber : phoneNumber});
        if(error !== '') {
            $('.error-phonenumber').text(error);
        }
        if(status) {
            $('.error-phonenumber').addClass('hide');
        } else {
            $('.error-phonenumber').removeClass('hide');
        }
        this.handleFrom();
        return true;
    }

    nameValid (e){
        let name = e.target.value;
        let error = '';
        let status = true;

        if(typeof name === 'undefined' || name === '') {
            status = false;
            error = 'Please Enter Name';
        }
        if(error !== '') {
            $('.error-name').text(error);
        }
        if(status) {
            $('.error-name').addClass('hide');
        } else {
            $('.error-name').removeClass('hide');
        }
        this.setState({name : name});
        this.handleFrom();
        return true;
    }

    
    reviewValid (e){
        let review = e.target.value;
        let error = '';
        let status = true;

        if(typeof review === 'undefined' || review === '' || review.length < 100) {
            status = false;
            error = 'Please Type Minimum 100 Characters';
        }
        if(error !== '') {
            $('.error-review').text(error);
        }
        if(status) {
            $('.error-review').addClass('hide');
            $('.world-count').removeClass('error');
            $('.world-count').addClass('green');

        } else {
            $('.error-review').removeClass('hide');
            $('.world-count').addClass('error');
            $('.world-count').removeClass('green');
        }
        let c=  ' ' + review.length + ' Characters';
        $('.world-count').text(c);
        this.setState({review : review});
        this.handleFrom();
        return true;
    }

    handleFrom(){ 
        setTimeout(() => {
            let status = true;

            if(this.state.rating === 0) {
                status = false;
            }

            if(this.state.phoneNumber.length !== 10) {
                status = false;
            }

            if(this.state.review.length < 100) {
                status = false;
            }

            if(this.state.name === '') {
                status = false;
            }

            if(status) {
                $('.wr-btn').prop("disabled", '');
            } else {
                $('.wr-btn').prop("disabled", 'disabled');
            }
            let o = {
                'review' : this.state.review,
                'rating' : this.state.rating,
                'name' : this.state.name,
                'phoneNumber' : this.state.phoneNumber,
                'formStatus' : status,
            };
            localStorage.setItem('writeReview',JSON.stringify(o));
            this.setState({formStatus : status});
        }, 50);
    }


    submit (e) {

        let self = this;
        e.preventDefault()
        if(this.state.formStatus) {
            $('.wr-btn').prop("disabled", 'disabled');
            let params = this.props.match.params;
            console.log(params)
            let d = [];
            d['review'] = this.state.review;
            d['rating'] = this.state.rating;
            d['name'] = this.state.name;
            d['phone_number'] = this.state.phoneNumber;
            d['id_model'] = params.model;
            Api.postData('postWriteReview', d, function(res){
                if(res.redirectUrl !== '') {
                    self.setState({
                        isPopup : true,
                        redirectUrl:res.redirectUrl
                    });
                    // window.location.href = res.redirectUrl;
                }
            });
        }
    }

    handleClose (e) {
        this.setState({isPopup : false});
        window.location.href = this.state.redirectUrl;
    }
    render() {
        let ratingChanged = (rating) => {

            this.setState({rating : rating});
            this.handleFrom();
        }

    	return (
            <div className=" write-review-page">
                {this.state.isPopup && <div id="myModal" className="review-popup">
                  <div className="review-popup-content">
                    <span className="close" onClick={(e) => this.handleClose(e)}>&times;</span>
                    <p>Thanks For Give Review</p>
                  </div>
                </div>}
                <h1>{this.state.title}</h1>
    		    <div className="row">
                    <div className="col-1 first">
                        <form action="" method="get" name="writeReview" id="writeReview" onSubmit={(e) => this.submit(e)}>
                            <ReactStars
                                count={5}
                                onChange={ratingChanged}
                                size={50}
                                isHalf={false}
                                emptyIcon={<i className="far fa-star"></i>}
                                halfIcon={<i className="fa fa-star-half-alt"></i>}
                                fullIcon={<i className="fa fa-star"></i>}
                                activeColor="#ffd700"
                                value={this.state.rating}
                              />
                            <div className="form-group">
                                <label >Review :- </label> 
                                <span className="world-count error" aria-live="polite"></span>
                                <textarea type="text" name="review" rows="4" className="form-control" placeholder="Enter Your Review" onChange={(e) => this.reviewValid(e)} value={this.state.review}/>
                                <span className="error error-review" aria-live="polite"></span>
                            </div>

                            <div className="form-group">
                                <label >Name</label>
                                <input type="text" name="name" className="form-control" placeholder="Enter Name" onChange={(e) => this.nameValid(e)} value={this.state.name}/>
                                <span className="error error-name" aria-live="polite"></span>
                            </div>

                            <div className="form-group">
                                <label >Phone Number</label>
                                <input type="number" maxLength="10" name="phoneNumber" className="form-control phoneNumber" placeholder="Enter PhoneNumber" onChange={(e) => this.phoneNumberValid(e)} value={this.state.phoneNumber}/>
                                <span className="error error-phonenumber" aria-live="polite"></span>
                            </div>

                            <button type="submit" className="btn btn-success wr-btn" disabled="true">SUBMIT</button>
                        </form>
                    </div>
                </div>
		    </div>
		);
    }
}
export default WriteReviewPage;
