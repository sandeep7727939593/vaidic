/**
 * Home.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
module.exports = {
	tableName : 'product',
    attributes: {
    	id: { 
	       type: 'number',  
	       columnName : 'id_product',
	       required: true,
	    },
        // id_product: { type: 'number', required: true },
        title: { 
        	type: 'string', 
        	required: true 
        },
        sub_title: { 
        	type: 'string', 
        	required: true 
        },
        description: { 
        	type: 'string'
        },
        link_rewrite: { 
        	type: 'string', 
        	required: true 
        },
        image: { 
        	type: 'string', 
        	required: true 
        },
        add_date: { 
        	type: 'string', 
        	required: true 
        },
        update_date: { 
        	type: 'string', 
        	required: true 
        },
        status: { 
        	type: 'number', 
        	required: true 
        },
    },
    getList : async (params = []) => {
    	let query = Product.selectQuery('product', params);
	    return await Product.getDatastore().sendNativeQuery(query, []);
	}
};

