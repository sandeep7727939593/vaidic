import React from 'react'
import './../static/css/ratingbar.css';
import Rating from './../widgets/Rating';
import configs from './../configs/configs';

const defaultProps = {
    review : {},
    color : ['#4CAF50', '#2196F3', '#00bcd4', '#ff9800', '#f44336'],
    height : '18px'
};

class ReviewCard extends React.Component {
    //Client Side
    constructor(props) {
        super(props);
        this.state = this.props ||  defaultProps;
        this.container = this.container.bind(this);
        this.reviewItems = this.reviewItems.bind(this);
    }

    container(reviewList) {
        let self = this;
        let text = [];
        for (var i = 0; i < reviewList.length; i++) {
            let css = {
                width: reviewList[i].per,
                height: self.state.height, 
                background: self.state.color[i],
            };
            let h = <div key={i}><div className="side">
                        <div>{reviewList[i].rating} star</div>
                    </div>
                    <div className="middle">
                    <div className="bar-container">
                        <div style={css}></div>
                    </div>
                    </div>
                    <div className="side right">
                        <div>{reviewList[i].review}</div>
                    </div></div>;
            text.push(h);
        }
            
        return text;
    }

    reviewItems(items) {
        let text = [];
        for(var i = 0; i < items.length; i++) {
            var item = items[i];
            let h = <li className="review-li" >
                <div >
                    <div className="review-top">
                        <img className="review-img" src={`${configs.imgUrl}/img/person.png`} alt="" width="50px" height="50px"/>
                    </div>
                    <div className="review-person">
                        <p className="review-title">{item.name}</p>
                        <Rating name="half-rating" defaultValue={2.5} precision={0.5} rating={item.rating}/>
                        <p>{item.add_date}</p>
                    </div>
                </div>
                <div className="review-desc">{item.review} </div>
            </li>;
            text.push(h);
        }
        return text;        

    }

    render() {

        

        if(!this.state.review.items) return null;
        let reviewGroup = this.state.review && this.state.review.reviewGroup ? this.state.review.reviewGroup : {};
        let items = this.state.review && this.state.review.items ? this.state.review.items : [];
        return (
            <div className="model-review-card">
                <h2 className="heading">{this.state.review.title}</h2>
                <Rating name="half-rating" defaultValue={2.5} precision={0.5} rating={this.state.review.rating}/>
                <p>{this.state.review.subTitle}</p>
                <hr />
                <div className="row">
                    {this.container(reviewGroup)}
                </div>
                <hr />
                <div className="row">
                    <ul>{this.reviewItems(items)}</ul>
                </div>
            </div>
        );
    }
}

ReviewCard.defaultProps = defaultProps;
export default ReviewCard;