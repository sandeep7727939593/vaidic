/**
 * Home.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
module.exports = {
    tableName : 'videos',
    attributes: {
        id: { 
           type: 'number',  
           columnName : 'id_videos',
           // required: true,
           unique:true,
           autoIncrement:true,
        },
        id_product: { 
            type: 'number', 
            required: true 
        },
        id_model: { 
            type: 'number', 
            required: true 
        },
        id_author: { 
            type: 'number', 
            required: true 
        },
        highlight: { 
            type: 'string', 
            required: true 
        },
        video_value: { 
            type: 'string', 
            required: true 
        },
        video_title: { 
            type: 'string'
        },
        link_rewrite: { 
            type: 'string'
        },
        view_count: { 
            type: 'string'
        },
        add_date: { 
            type: 'ref', 
            required: true 
        },
        update_date: { 
            type: 'ref', 
            required: true 
        },
        status: { 
            type: 'number', 
            required: true 
        },
        video_caption: { 
            type: 'string', 
            required: true 
        },
    },
    getList : async (params = []) => {
        let query = Videos.selectQuery('videos', params);
        return await Videos.getDatastore().sendNativeQuery(query, []);
    }
};

