/**
 * Specs.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
module.exports = {
    tableName : 'specs',
    attributes: {
        id: { 
           type: 'number',  
           columnName : 'id_specs',
           required: true,
        },
        id_product: { 
            type: 'number', 
            required: true 
        },
        id_model: { 
            type: 'number', 
            required: true 
        },
        specs: { 
            type: 'string'
        },
        add_date: { 
            type: 'string', 
            required: true 
        },
        update_date: { 
            type: 'string', 
            required: true 
        },
        status: { 
            type: 'number', 
            required: true 
        }
    },
    getList : async (params = []) => {
        let query = Specs.selectQuery('specs', params);
        return await Specs.getDatastore().sendNativeQuery(query, []);
    }
};

