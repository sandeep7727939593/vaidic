import React from 'react';
// import Search from './../widgets/Search';
import { Carousel} from 'react-bootstrap';


const HomeBanner = (props) => {
	//Client Side
   	if(!props.banner) {return null};
	let bannerImage = () => {
		let banner = props.banner;
		if(!banner) return null;
		let h = [];
		banner.map((item, index) => 
			h.push(
					<Carousel.Item 
						interval={5000} 
						key={index} 
						className={`carousel-item 
						${item.active ? 'active' : ''}`}>
								<div className="img-block bg-overlay-gradient-04" data-autofit="true" 
								/* style={{ backgroundImage: `url(${item.img})`, height: "600px" }} */
								>
										<img 
											src={item.img} 
											alt={item.title} 
											className="d-block w-100"
										/>	
									</div>	
									{/* <Carousel.Caption>
										<h3>First slide label</h3>
										<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
									</Carousel.Caption> */}
						</Carousel.Item>
			)
		); 
		return h;
	}
	return (
		<>
			<section className="clearfix">
					{/* <div className="inner-banner-1">
						<Search />
					</div> */}
				{/* <div id="slider" className="carousel slide" data-ride="carousel">
					<div className="carousel-inner">
						{bannerImage()}
					</div>
    </div> */}
			<div className="mainslider">
					<Carousel>
						{bannerImage()}
					</Carousel>
			</div>
			</section>
			 </>
	);
}
export default HomeBanner;
