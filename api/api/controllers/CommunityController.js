/**
 * CommunityController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
	getCommunity : async (req, res) => {
		// http://localhost:1337/api/getCommunity?pageSize=2&pageNo=2
		res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

		let pageSize = req.query.pageSize ? req.query.pageSize : 5;
		let pageNo = req.query.pageNo ? req.query.pageNo : 1;

		let params = {
			filters : [
				'status=1',
			],
		};
		var countData = await Community.getList(params);
		params.limit = pageSize;
		params.page = pageNo;
		var data = await Community.getList(params);
		var result = {};
		if(!sails.hooks.common.isEmptyObject(data.rows)) {
			countData = countData.rows.length;
			result = {
				items : data.rows,
				pagination : {
					totalCount : countData,
					pageNo : pageNo,
					pageSize : pageSize,
					pageCount : Math.ceil(countData/pageSize)
				},
				title : 'Our Social Posts',
				seoData : {
					title : 'Our Social Posts',
					description : 'Our Social Posts @' + sails.config.domainName ,
					index : '',
				},
				url : sails.hooks.common.communityUrl(false),
			};
		}
		res.send(result);
	}
};

