import React from 'react';
import Table from 'react-bootstrap/Table';
import './../static/css/specification.css';

const Specification = (props) => {
	if(typeof props.data.title === 'undefined') return null;
	let items = props.data.items ? props.data.items : [];
	let title = props.data.title ? props.data.title : '';
	return (
		<div className="specification">
			<h2>{title}</h2>
			<Table striped bordered hover size="xl">
			  <thead>
			    <tr>
			      <th>Specification Type</th>
			      <th>Value</th>
			    </tr>
			  </thead>
			  <tbody>
			  {items.map((item, index) => 
				<tr>
			      <td>{item.name}</td>
			      <td>{item.value}</td>
			    </tr>
				)}
			    
			  </tbody>
			</Table>
		</div>
	);
}
export default Specification;
