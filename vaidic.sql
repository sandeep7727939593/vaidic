-- MariaDB dump 10.17  Distrib 10.4.14-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: vaidic
-- ------------------------------------------------------
-- Server version	10.4.14-MariaDB-1:10.4.14+maria~bionic-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Advisors`
--

DROP TABLE IF EXISTS `Advisors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Advisors` (
  `Advisor_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Advisor_Name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Advisor_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Advisors`
--

LOCK TABLES `Advisors` WRITE;
/*!40000 ALTER TABLE `Advisors` DISABLE KEYS */;
INSERT INTO `Advisors` VALUES (1,'a1'),(2,'a2'),(3,'a3');
/*!40000 ALTER TABLE `Advisors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Students`
--

DROP TABLE IF EXISTS `Students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Students` (
  `Student_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Student_Name` varchar(100) DEFAULT NULL,
  `Advisor_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Student_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Students`
--

LOCK TABLES `Students` WRITE;
/*!40000 ALTER TABLE `Students` DISABLE KEYS */;
INSERT INTO `Students` VALUES (1,'a',1),(2,'ab',2),(3,'abc',10);
/*!40000 ALTER TABLE `Students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `idx-auth_assignment-user_id` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('Super Admin','12734306',1591520172);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('/*',2,NULL,NULL,NULL,1592654880,1592654880),('/admin/*',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/assignment/*',2,NULL,NULL,NULL,1591520282,1591520282),('/admin/assignment/assign',2,NULL,NULL,NULL,1591520282,1591520282),('/admin/assignment/index',2,NULL,NULL,NULL,1591520282,1591520282),('/admin/assignment/revoke',2,NULL,NULL,NULL,1591520282,1591520282),('/admin/assignment/view',2,NULL,NULL,NULL,1591520282,1591520282),('/admin/default/*',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/default/index',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/menu/*',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/menu/create',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/menu/delete',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/menu/index',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/menu/update',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/menu/view',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/permission/*',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/permission/assign',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/permission/create',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/permission/delete',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/permission/index',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/permission/remove',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/permission/update',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/permission/view',2,NULL,NULL,NULL,1591520283,1591520283),('/admin/role/*',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/role/assign',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/role/create',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/role/delete',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/role/index',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/role/remove',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/role/update',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/role/view',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/route/*',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/route/assign',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/route/create',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/route/index',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/route/refresh',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/route/remove',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/rule/*',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/rule/create',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/rule/delete',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/rule/index',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/rule/update',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/rule/view',2,NULL,NULL,NULL,1591520284,1591520284),('/admin/user/*',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/user/activate',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/user/change-password',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/user/delete',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/user/index',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/user/login',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/user/logout',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/user/request-password-reset',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/user/reset-password',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/user/signup',2,NULL,NULL,NULL,1591520285,1591520285),('/admin/user/view',2,NULL,NULL,NULL,1591520285,1591520285),('/category/*',2,NULL,NULL,NULL,1591545374,1591545374),('/category/create',2,NULL,NULL,NULL,1591545373,1591545373),('/category/delete',2,NULL,NULL,NULL,1591545374,1591545374),('/category/index',2,NULL,NULL,NULL,1591545373,1591545373),('/category/update',2,NULL,NULL,NULL,1591545374,1591545374),('/category/view',2,NULL,NULL,NULL,1591545373,1591545373),('/common/*',2,NULL,NULL,NULL,1607265459,1607265459),('/faq/*',2,NULL,NULL,NULL,1611158980,1611158980),('/faq/create',2,NULL,NULL,NULL,1611158980,1611158980),('/faq/delete',2,NULL,NULL,NULL,1611158980,1611158980),('/faq/index',2,NULL,NULL,NULL,1611158980,1611158980),('/faq/update',2,NULL,NULL,NULL,1611158980,1611158980),('/faq/view',2,NULL,NULL,NULL,1611158980,1611158980),('/gii/*',2,NULL,NULL,NULL,1591540094,1591540094),('/gii/default/*',2,NULL,NULL,NULL,1591540124,1591540124),('/gii/default/action',2,NULL,NULL,NULL,1591540124,1591540124),('/gii/default/diff',2,NULL,NULL,NULL,1591540124,1591540124),('/gii/default/index',2,NULL,NULL,NULL,1591540124,1591540124),('/gii/default/preview',2,NULL,NULL,NULL,1591540124,1591540124),('/gii/default/view',2,NULL,NULL,NULL,1591540124,1591540124),('/model/*',2,NULL,NULL,NULL,1600009663,1600009663),('/model/create',2,NULL,NULL,NULL,1600009663,1600009663),('/model/delete',2,NULL,NULL,NULL,1600009663,1600009663),('/model/index',2,NULL,NULL,NULL,1600009663,1600009663),('/model/update',2,NULL,NULL,NULL,1600009663,1600009663),('/model/view',2,NULL,NULL,NULL,1600009663,1600009663),('/news/*',2,NULL,NULL,NULL,1608744683,1608744683),('/news/create',2,NULL,NULL,NULL,1608744682,1608744682),('/news/delete',2,NULL,NULL,NULL,1608744682,1608744682),('/news/index',2,NULL,NULL,NULL,1608744680,1608744680),('/news/update',2,NULL,NULL,NULL,1608744682,1608744682),('/news/view',2,NULL,NULL,NULL,1608744681,1608744681),('/product/*',2,NULL,NULL,NULL,1599671471,1599671471),('/product/create',2,NULL,NULL,NULL,1599671470,1599671470),('/product/delete',2,NULL,NULL,NULL,1599671471,1599671471),('/product/index',2,NULL,NULL,NULL,1599671470,1599671470),('/product/update',2,NULL,NULL,NULL,1599671470,1599671470),('/product/view',2,NULL,NULL,NULL,1599671470,1599671470),('/specs/*',2,NULL,NULL,NULL,1611502588,1611502588),('/specs/create',2,NULL,NULL,NULL,1611502588,1611502588),('/specs/delete',2,NULL,NULL,NULL,1611502588,1611502588),('/specs/index',2,NULL,NULL,NULL,1611502586,1611502586),('/specs/update',2,NULL,NULL,NULL,1611502588,1611502588),('/specs/view',2,NULL,NULL,NULL,1611502587,1611502587),('/topic/*',2,NULL,NULL,NULL,1592654880,1592654880),('/topic/create',2,NULL,NULL,NULL,1592654880,1592654880),('/topic/delete',2,NULL,NULL,NULL,1592654880,1592654880),('/topic/index',2,NULL,NULL,NULL,1592654880,1592654880),('/topic/update',2,NULL,NULL,NULL,1592654880,1592654880),('/topic/view',2,NULL,NULL,NULL,1592654880,1592654880),('/videos/*',2,NULL,NULL,NULL,1607264103,1607264103),('/videos/create',2,NULL,NULL,NULL,1607264103,1607264103),('/videos/delete',2,NULL,NULL,NULL,1607264103,1607264103),('/videos/index',2,NULL,NULL,NULL,1607264103,1607264103),('/videos/update',2,NULL,NULL,NULL,1607264103,1607264103),('/videos/view',2,NULL,NULL,NULL,1607264103,1607264103),('manageAdmin',2,NULL,NULL,NULL,1591520362,1591520362),('manageCategory',2,NULL,NULL,NULL,1591545380,1591545380),('manageCommon',2,NULL,NULL,NULL,1607265474,1607265474),('manageFaq',2,NULL,NULL,NULL,1611158987,1611158987),('manageGii',2,NULL,NULL,NULL,1591540136,1591540136),('manageModel',2,NULL,NULL,NULL,1600009674,1600009674),('manageNews',2,NULL,NULL,NULL,1608744694,1608744694),('manageProduct',2,NULL,NULL,NULL,1599671514,1599671514),('manageSpecs',2,NULL,NULL,NULL,1611502613,1611502613),('manageTopic',2,NULL,NULL,NULL,1592654886,1592654886),('manageVideos',2,NULL,NULL,NULL,1607264112,1607264112),('Super Admin',1,NULL,NULL,NULL,1591520161,1591520161);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('manageAdmin','/admin/*'),('manageAdmin','/admin/assignment/*'),('manageAdmin','/admin/assignment/assign'),('manageAdmin','/admin/assignment/index'),('manageAdmin','/admin/assignment/revoke'),('manageAdmin','/admin/assignment/view'),('manageAdmin','/admin/default/*'),('manageAdmin','/admin/default/index'),('manageAdmin','/admin/menu/*'),('manageAdmin','/admin/menu/create'),('manageAdmin','/admin/menu/delete'),('manageAdmin','/admin/menu/index'),('manageAdmin','/admin/menu/update'),('manageAdmin','/admin/menu/view'),('manageAdmin','/admin/permission/*'),('manageAdmin','/admin/permission/assign'),('manageAdmin','/admin/permission/create'),('manageAdmin','/admin/permission/delete'),('manageAdmin','/admin/permission/index'),('manageAdmin','/admin/permission/remove'),('manageAdmin','/admin/permission/update'),('manageAdmin','/admin/permission/view'),('manageAdmin','/admin/role/*'),('manageAdmin','/admin/role/assign'),('manageAdmin','/admin/role/create'),('manageAdmin','/admin/role/delete'),('manageAdmin','/admin/role/index'),('manageAdmin','/admin/role/remove'),('manageAdmin','/admin/role/update'),('manageAdmin','/admin/role/view'),('manageAdmin','/admin/route/*'),('manageAdmin','/admin/route/assign'),('manageAdmin','/admin/route/create'),('manageAdmin','/admin/route/index'),('manageAdmin','/admin/route/refresh'),('manageAdmin','/admin/route/remove'),('manageAdmin','/admin/rule/*'),('manageAdmin','/admin/rule/create'),('manageAdmin','/admin/rule/delete'),('manageAdmin','/admin/rule/index'),('manageAdmin','/admin/rule/update'),('manageAdmin','/admin/rule/view'),('manageAdmin','/admin/user/*'),('manageAdmin','/admin/user/activate'),('manageAdmin','/admin/user/change-password'),('manageAdmin','/admin/user/delete'),('manageAdmin','/admin/user/index'),('manageAdmin','/admin/user/login'),('manageAdmin','/admin/user/logout'),('manageAdmin','/admin/user/request-password-reset'),('manageAdmin','/admin/user/reset-password'),('manageAdmin','/admin/user/signup'),('manageAdmin','/admin/user/view'),('manageCategory','/category/*'),('manageCategory','/category/create'),('manageCategory','/category/delete'),('manageCategory','/category/index'),('manageCategory','/category/update'),('manageCategory','/category/view'),('manageCommon','/common/*'),('manageFaq','/faq/*'),('manageFaq','/faq/create'),('manageFaq','/faq/delete'),('manageFaq','/faq/index'),('manageFaq','/faq/update'),('manageFaq','/faq/view'),('manageGii','/gii/*'),('manageGii','/gii/default/*'),('manageGii','/gii/default/action'),('manageGii','/gii/default/diff'),('manageGii','/gii/default/index'),('manageGii','/gii/default/preview'),('manageGii','/gii/default/view'),('manageModel','/model/*'),('manageModel','/model/create'),('manageModel','/model/delete'),('manageModel','/model/index'),('manageModel','/model/update'),('manageModel','/model/view'),('manageNews','/news/*'),('manageNews','/news/create'),('manageNews','/news/delete'),('manageNews','/news/index'),('manageNews','/news/update'),('manageNews','/news/view'),('manageProduct','/product/*'),('manageProduct','/product/create'),('manageProduct','/product/delete'),('manageProduct','/product/index'),('manageProduct','/product/update'),('manageProduct','/product/view'),('manageSpecs','/specs/*'),('manageSpecs','/specs/create'),('manageSpecs','/specs/delete'),('manageSpecs','/specs/index'),('manageSpecs','/specs/update'),('manageSpecs','/specs/view'),('manageTopic','/topic/*'),('manageTopic','/topic/create'),('manageTopic','/topic/delete'),('manageTopic','/topic/index'),('manageTopic','/topic/update'),('manageTopic','/topic/view'),('manageVideos','/videos/*'),('manageVideos','/videos/create'),('manageVideos','/videos/delete'),('manageVideos','/videos/index'),('manageVideos','/videos/update'),('manageVideos','/videos/view'),('Super Admin','manageAdmin'),('Super Admin','manageCategory'),('Super Admin','manageCommon'),('Super Admin','manageFaq'),('Super Admin','manageGii'),('Super Admin','manageModel'),('Super Admin','manageNews'),('Super Admin','manageProduct'),('Super Admin','manageSpecs'),('Super Admin','manageVideos');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author` (
  `id_author` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key for the table',
  `email_id` varchar(45) NOT NULL COMMENT 'Email id of the author',
  `twitter` varchar(45) DEFAULT NULL,
  `linked_in` varchar(45) DEFAULT NULL,
  `description` longtext NOT NULL COMMENT 'Description about the author',
  `image_url` varchar(255) DEFAULT NULL COMMENT 'Provides image url of the author',
  `no_of_viewers` int(11) NOT NULL DEFAULT 0,
  `author_email_id` varchar(45) DEFAULT NULL,
  `start_date` datetime NOT NULL COMMENT 'Provide registration date for author',
  `end_date` datetime DEFAULT '2025-01-01 00:00:00' COMMENT 'Provide end date of author',
  `update_date` datetime NOT NULL COMMENT 'Provide update_date of the author',
  `name` varchar(255) NOT NULL,
  `link_rewrite` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `mobileno` varchar(30) DEFAULT NULL,
  `is_blogger` tinyint(4) DEFAULT 1,
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `updated_by_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_author`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1,'team@gmail.com','t','l','','',0,NULL,'2020-01-01 00:00:00','2025-01-01 00:00:00','2020-12-12 00:00:00','Team ChakkiWale','team-chakkiwale','Jaipur','7727939593',0,NULL,NULL);
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) NOT NULL,
  `link_rewrite` varchar(500) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_by_id` varchar(50) DEFAULT NULL,
  `updated_by_id` varchar(50) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq` (
  `id_faq` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) DEFAULT NULL,
  `id_model` int(11) DEFAULT NULL,
  `link_rewrite` varchar(50) DEFAULT NULL,
  `question` varchar(500) DEFAULT NULL,
  `answer` text DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `launch_date` datetime DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by_id` varchar(50) DEFAULT NULL,
  `updated_by_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_faq`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` VALUES (1,5,0,'how-to-make-mashala-','How to make Mashala ?','This is a quick Garam Masala (Indian spice) mix. Garam Masala is better when made with whole spices that have been roasted and ground, but this is a quick and easy substitute that\'s pretty good.',1,'2021-01-20 16:20:24',NULL,'2021-01-20 11:06:32',NULL,NULL),(2,4,0,'what-type-of-floor','What type of floor?','Materials almost always classified as floor covering include carpet, area rugs, and resilient flooring such as linoleum or vinyl flooring. ... Materials commonly called flooring include wood flooring, laminated wood, ceramic tile, stone, terrazzo, and various seamless chemical floor coatings',1,'2021-01-20 16:37:34',NULL,'2021-01-20 11:07:34',NULL,NULL);
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (2,'Manage Assignments',4,'/admin/assignment/index',NULL,NULL),(3,'Manage Menu',4,'/admin/menu/index',NULL,NULL),(4,'Manage Admin',NULL,NULL,NULL,NULL),(5,'Manage Permission',4,'/admin/permission/index',NULL,NULL),(6,'Manage Route',4,'/admin/route/index',NULL,NULL),(7,'Manage Role',4,'/admin/role/index',NULL,NULL),(8,'Manage Category',NULL,'/category/index',NULL,NULL),(9,'Manage Topic',NULL,'/topic/index',NULL,NULL),(10,'Manage Product',NULL,'/product/index',NULL,NULL),(11,'Manage Model',NULL,'/model/index',NULL,NULL),(12,'Manage Videos',NULL,'/videos/index',NULL,NULL),(13,'Manage News',NULL,'/news/index',NULL,NULL),(14,'Manage Faq',NULL,'/faq/index',NULL,NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1591064939),('m140506_102106_rbac_init',1591065332),('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1591065333),('m180523_151638_rbac_updates_indexes_without_prefix',1591065335),('m200409_110543_rbac_update_mssql_trigger',1591065335);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model` (
  `id_model` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) DEFAULT NULL,
  `cover_image` varchar(500) DEFAULT NULL,
  `model_name` varchar(50) DEFAULT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `link_rewrite` varchar(50) DEFAULT NULL,
  `base_price` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `popularity` int(11) DEFAULT NULL,
  `alternative_link_rewrite` varchar(100) DEFAULT NULL,
  `alternative_link_type` varchar(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `launch_date` datetime DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by_id` varchar(50) DEFAULT NULL,
  `updated_by_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_model`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model`
--

LOCK TABLES `model` WRITE;
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
INSERT INTO `model` VALUES (3,5,'download.jpeg','Mrich Paudar','Mrich','mrich',431,'<p>Kashmiri chilly powder is prepared from top quality Kasmhmiri chillies, so that it maintains its traditional flavour and texture. High quality raw materials are procured from farmers and are carefully washed, dried and pounded in making this Kashmiri chilly powder. &nbsp;All of our spice blends are freshly grounded products without any of the artificial preservatives. Try out this 100% genuine Kashmiri Chilly powder.</p>',2,'','',1,'2020-09-14 17:38:50','2020-09-23 00:00:00','2020-10-03 08:45:08',NULL,NULL),(4,4,'atta-chakki-machine-1465434.jpg','Hand Chakki','Hand Chakki','hand-chakki',25000,'<p>sdadsfdfdsfsdf</p>',21,'','',1,'2020-09-14 17:56:01','2020-09-16 00:00:00','2020-09-26 06:05:35',NULL,NULL),(5,4,'logo.jpg','Wood Chakki','Wood Chakki','wood-chakki',35000,'<p>Wood ChakkiWood ChakkiWood ChakkiWood ChakkiWood ChakkiWood ChakkiWood ChakkiWood ChakkiWood ChakkiWood ChakkiWood ChakkiWood ChakkiWood ChakkiWood ChakkiWood Chakki</p>',12,NULL,NULL,1,'2020-10-03 13:02:56','2020-10-12 00:00:00','2020-10-03 07:32:56',NULL,NULL),(6,5,'Tumeric_732x549-thumbnail.jpg','Pisi huii Haldi','Haldi','pisi-huii-haldi',400,'<p>These compounds are called curcuminoids, the most important of which is curcumin.</p>\r\n<p>Curcumin is the main active ingredient in turmeric. It has powerful anti-inflammatory effects and is a very strong antioxidant.</p>\r\n<p>However, the curcumin content of turmeric is not that high. It&rsquo;s around 3%, by weight (2Trusted Source).</p>\r\n<p>Most of the studies on this herb are using turmeric extracts that contain mostly curcumin itself, with dosages usually exceeding 1 gram per day.</p>\r\n<p>It would be very difficult to reach these levels just using the turmeric spice in your foods.</p>\r\n<p>Therefore, if you want to experience the full effects, you need to take a supplement that contains significant amounts of curcumin.</p>\r\n<p>Unfortunately, curcumin is poorly absorbed into the bloodstream. It helps to consume black pepper with it, which contains piperine, a natural substance that enhances the absorption of curcumin by 2,000% (3Trusted Source).</p>\r\n<p>The best curcumin supplements contain piperine, substantially increasing their effectiveness.</p>\r\n<p>Curcumin is also fat soluble, so it may be a good idea to take it with a fatty meal.</p>\r\n<p>SUMMARY<br />Turmeric contains curcumin, a substance with powerful anti-inflammatory and antioxidant properties. Most studies used turmeric extracts that are standardized to include large amounts of curcumin.</p>\r\n<p><br />2. Curcumin Is a Natural Anti-Inflammatory Compound<br />Inflammation is incredibly important.</p>\r\n<p>It helps your body fight foreign invaders and also has a role in repairing damage.</p>\r\n<p>Without inflammation, pathogens like bacteria could easily take over your body and kill you.</p>\r\n<p>Although acute, short-term inflammation is beneficial, it can become a major problem when it becomes chronic and inappropriately attacks your body&rsquo;s own tissues.</p>\r\n<p>Scientists now believe that chronic, low-level inflammation plays a major role in almost every chronic, Western disease. This includes heart disease, cancer, metabolic syndrome, Alzheimer&rsquo;s and various degenerative conditions (4Trusted Source, 5Trusted Source, 6Trusted Source).</p>\r\n<p>Therefore, anything that can help fight chronic inflammation is of potential importance in preventing and even treating these diseases.</p>\r\n<p>Curcumin is strongly anti-inflammatory. In fact, it&rsquo;s so powerful that it matches the effectiveness of some anti-inflammatory drugs, without the side effects (7Trusted Source, 8Trusted Source, 9Trusted Source ).</p>\r\n<p>It blocks NF-kB, a molecule that travels into the nuclei of your cells and turns on genes related to inflammation. NF-kB is believed to play a major role in many chronic diseases (10, 11Trusted Source).</p>\r\n<p>Without getting into the details (inflammation is extremely complicated), the key takeaway is that curcumin is a bioactive substance that fights inflammation at the molecular level (12Trusted Source, 13, 14).</p>\r\n<p>SUMMARY<br />Chronic inflammation contributes to many common Western diseases. Curcumin can suppress many molecules known to play major roles in inflammation.</p>\r\n<p>3. Turmeric Dramatically Increases the Antioxidant Capacity of the Body<br />Oxidative damage is believed to be one of the mechanisms behind aging and many diseases.</p>\r\n<p>It involves free radicals, highly reactive molecules with unpaired electrons.</p>\r\n<p>Free radicals tend to react with important organic substances, such as fatty acids, proteins or DNA.</p>\r\n<p>The main reason antioxidants are so beneficial is that they protect your body from free radicals.</p>\r\n<p>Curcumin is a potent antioxidant that can neutralize free radicals due to its chemical structure (15Trusted Source, 16Trusted Source).</p>\r\n<p>In addition, curcumin boosts the activity of your body&rsquo;s own antioxidant enzymes (17, 18, 19Trusted Source).</p>\r\n<p>In that way, curcumin delivers a one-two punch against free radicals. It blocks them directly, then stimulates your body&rsquo;s own antioxidant defenses.</p>\r\n<p>SUMMARY<br />Curcumin has powerful antioxidant effects. It neutralizes free radicals on its own but also stimulates your body&rsquo;s own antioxidant enzymes.</p>\r\n<p>4. Curcumin Boosts Brain-Derived Neurotrophic Factor, Linked to Improved Brain Function and a Lower Risk of Brain Diseases<br />Back in the day, it was believed that neurons weren&rsquo;t able to divide and multiply after early childhood.</p>\r\n<p>However, it&rsquo;s now known that this does happen.</p>\r\n<p>Neurons are capable of forming new connections, but in certain areas of the brain they can also multiply and increase in number.</p>\r\n<p>One of the main drivers of this process is brain-derived neurotrophic factor (BDNF), which is a type of growth hormone that functions in your brain (20Trusted Source).</p>\r\n<p>Many common brain disorders have been linked to decreased levels of this hormone, including depression and Alzheimer&rsquo;s disease (21, 22).</p>\r\n<p>Interestingly, curcumin can increase brain levels of BDNF (23, 24).</p>\r\n<p>By doing this, it may be effective in delaying or even reversing many brain diseases and age-related decreases in brain function (25Trusted Source).</p>\r\n<p>It may also improve memory and make you smarter, which seems logical given its effects on BDNF levels. However, controlled studies in people are needed to confirm this (26).</p>\r\n<p>SUMMARY<br />Curcumin boosts levels of the brain hormone BDNF, which increases the growth of new neurons and fights various degenerative processes in your brain.</p>\r\n<p><br />5. Curcumin Should Lower Your Risk of Heart Disease<br />Heart disease is the number 1 cause of death in the world (27Trusted Source).</p>\r\n<p>Researchers have studied it for many decades and learned a lot about why it happens.</p>\r\n<p>Unsurprisingly, heart disease is incredibly complicated and various things contribute to it.</p>\r\n<p>Curcumin may help reverse many steps in the heart disease process (28Trusted Source).</p>\r\n<p>Perhaps the main benefit of curcumin when it comes to heart disease is improving the function of the endothelium, which is the lining of your blood vessels.</p>\r\n<p>It&rsquo;s well known that endothelial dysfunction is a major driver of heart disease and involves an inability of your endothelium to regulate blood pressure, blood clotting and various other factors (29Trusted Source).</p>\r\n<p>Several studies suggest that curcumin leads to improvements in endothelial function. One study found that it&rsquo;s as effective as exercise while another shows that it works as well as the drug Atorvastatin (30Trusted Source, 31Trusted Source).</p>\r\n<p>In addition, curcumin reduces inflammation and oxidation (as discussed above), which play a role in heart disease as well.</p>\r\n<p>One study randomly assigned 121 people, who were undergoing coronary artery bypass surgery, either a placebo or 4 grams of curcumin per day, a few days before and after the surgery.</p>\r\n<p>The curcumin group had a 65% decreased risk of experiencing a heart attack in the hospital (32Trusted Source).</p>\r\n<p>SUMMARY<br />Curcumin has beneficial effects on several factors known to play a role in heart disease. It improves the function of the endothelium and is a potent anti-inflammatory agent and antioxidant.</p>\r\n<p>6. Turmeric Can Help Prevent (And Perhaps Even Treat) Cancer<br />Cancer is a terrible disease, characterized by uncontrolled cell growth.</p>\r\n<p>There are many different forms of cancer, which still have several things in common. Some of them appear to be affected by curcumin supplements (33Trusted Source).</p>\r\n<p>Curcumin has been studied as a beneficial herb in cancer treatment and been found to affect cancer growth, development and spread at the molecular level (34Trusted Source).</p>\r\n<p>Studies have shown that it can contribute to the death of cancerous cells and reduce angiogenesis (growth of new blood vessels in tumors) and metastasis (spread of cancer) (35Trusted Source).</p>\r\n<p>Multiple studies indicate that curcumin can reduce the growth of cancerous cells in the laboratory and inhibit the growth of tumors in test animals (36Trusted Source, 37Trusted Source).</p>\r\n<p>Whether high-dose curcumin (preferably with an absorption enhancer like piperine) can help treat cancer in humans has yet to be studied properly.</p>\r\n<p>However, there is evidence that it may prevent cancer from occurring in the first place, especially cancers of the digestive system like colorectal cancer.</p>\r\n<p>In a 30-day study in 44 men with lesions in the colon that sometimes turn cancerous, 4 grams of curcumin per day reduced the number of lesions by 40% (38Trusted Source).</p>\r\n<p>Maybe curcumin will be used along with conventional cancer treatment one day. It&rsquo;s too early to say for sure, but it looks promising and is being intensively studied.</p>\r\n<p>SUMMARY<br />Curcumin leads to several changes on the molecular level that may help prevent and perhaps even treat cancer.</p>\r\n<p>7. Curcumin May Be Useful in Preventing and Treating Alzheimer&rsquo;s Disease<br />Alzheimer&rsquo;s disease is the most common neurodegenerative disease in the world and a leading cause of dementia.</p>\r\n<p>Unfortunately, no good treatment is available for Alzheimer&rsquo;s yet.</p>\r\n<p>Therefore, preventing it from occurring in the first place is of utmost importance.</p>\r\n<p>There may be good news on the horizon because curcumin has been shown to cross the blood-brain barrier (39Trusted Source).</p>\r\n<p>It&rsquo;s known that inflammation and oxidative damage play a role in Alzheimer&rsquo;s disease, and curcumin has beneficial effects on both (40).</p>\r\n<p>In addition, a key feature of Alzheimer&rsquo;s disease is a buildup of protein tangles called amyloid plaques. Studies show that curcumin can help clear these plaques (41Trusted Source).</p>\r\n<p>Whether curcumin can really slow down or even reverse the progression of Alzheimer&rsquo;s disease in people is currently unknown and needs to be studied properly.</p>\r\n<p>SUMMARY<br />Curcumin can cross the blood-brain barrier and has been shown to lead to various improvements in the pathological process of Alzheimer&rsquo;s disease.</p>\r\n<p>8. Arthritis Patients Respond Very Well to Curcumin Supplements<br />Arthritis is a common problem in Western countries.</p>\r\n<p>There are several different types, most of which involve inflammation in the joints.</p>\r\n<p>Given that curcumin is a potent anti-inflammatory compound, it makes sense that it may help with arthritis.</p>\r\n<p>Several studies show this to be true.</p>\r\n<p>In a study in people with rheumatoid arthritis, curcumin was even more effective than an anti-inflammatory drug (42Trusted Source).</p>\r\n<p>Many other studies have looked at the effects of curcumin on arthritis and noted improvements in various symptoms (43Trusted Source, 44Trusted Source).</p>\r\n<p>SUMMARY<br />Arthritis is a common disorder characterized by joint inflammation. Many studies show that curcumin can help treat symptoms of arthritis and is in some cases more effective than anti-inflammatory drugs.</p>\r\n<p>9. Studies Show That Curcumin Has Incredible Benefits Against Depression<br />Curcumin has shown some promise in treating depression.</p>\r\n<p>In a controlled trial, 60 people with depression were randomized into three groups (45Trusted Source).</p>\r\n<p>One group took Prozac, another group one gram of curcumin and the third group both Prozac and curcumin.</p>\r\n<p>After 6 weeks, curcumin had led to improvements that were similar to Prozac. The group that took both Prozac and curcumin fared best (45Trusted Source).</p>\r\n<p>According to this small study, curcumin is as effective as an antidepressant.</p>\r\n<p>Depression is also linked to reduced levels of brain-derived neurotrophic factor (BDNF) and a shrinking hippocampus, a brain area with a role in learning and memory.</p>\r\n<p>Curcumin boosts BDNF levels, potentially reversing some of these changes (46).</p>\r\n<p>There is also some evidence that curcumin can boost the brain neurotransmitters serotonin and dopamine (47, 48).</p>\r\n<p>SUMMARY<br />A study in 60 people with depression showed that curcumin was as effective as Prozac in alleviating symptoms of the condition.</p>\r\n<p>10. Curcumin May Help Delay Aging and Fight Age-Related Chronic Diseases<br />If curcumin can really help prevent heart disease, cancer and Alzheimer&rsquo;s, it would have obvious benefits for longevity.</p>\r\n<p>For this reason, curcumin has become very popular as an anti-aging supplement (49Trusted Source).</p>\r\n<p>But given that oxidation and inflammation are believed to play a role in aging, curcumin may have effects that go way beyond just preventing disease (50Trusted Source).</p>\r\n<p>SUMMARY<br />Due to its many positive health effects, such as the potential to prevent heart disease, Alzheimer&rsquo;s and cancer, curcumin may aid longevity.</p>\r\n<p>The Bottom Line<br />Turmeric and especially its most active compound curcumin have many scientifically-proven health benefits, such as the potential to prevent heart disease, Alzheimer&rsquo;s and cancer.</p>\r\n<p>It&rsquo;s a potent anti-inflammatory and antioxidant and may also help improve symptoms of depression and arthritis.</p>\r\n<p>If you want to buy a turmeric/curcumin supplement, there is an excellent selection on Amazon with thousands of great customer reviews.</p>\r\n<p>It&rsquo;s recommended to find a product with BioPerine (the trademarked name for piperine), which is the substance that enhances curcumin absorption by 2,000%.</p>\r\n<p>Without this substance, most of the curcumin just passes through your digestive tract.</p>\r\n<p>Written by Kris Gunnars, BSc on July 13, 2018</p>\r\n<p>related stories<br />Does Too Much Turmeric Have Side Effects?</p>',123,NULL,NULL,1,'2020-10-03 13:59:11','2020-10-12 00:00:00','2020-10-03 08:29:11',NULL,NULL),(7,5,'images.jpeg','Pisha huaa Jira','JIra','pisha-huaa-jira',200,'<p>Cumin comes from a flowering parsley plant and is used in many cuisines for its earthy citrusy flavor. It has medicinal value as well. Cumin contains rich amount of iron, vitamins B-complex, E, A, and C, and antioxidants. This spice promotes digestion, helps maintain diabetes and cholesterol, and reduces risk of infections.</p>',21,NULL,NULL,1,'2020-10-03 14:01:29','2020-10-12 00:00:00','2020-10-03 08:31:29',NULL,NULL),(8,5,'coriander-masala-powder-500x500.jpeg','Dhaniya Paudar','Dhaniya','dhaniya-paudar',321,'<p>Established in the year 2015, we &ldquo;Laal Amrood&rdquo; are a Partnership Firm, engaged in wholesale trading and retailing an extensive range of Fresh Mango, Fresh Guava, Fresh Garlic, Fresh Ginger and Masala Powder.Located at Allahabad, (Uttar Pradesh, India), we have set up strong vendor base. Under the stern guidance of our mentor &ldquo;Helal Khan (Partner)&rdquo;, we have been able to attain dynamic position in the market.</p>',213,NULL,NULL,1,'2020-10-03 14:03:13','2020-10-12 00:00:00','2020-10-03 08:33:13',NULL,NULL);
/*!40000 ALTER TABLE `model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id_news` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `id_model` int(11) NOT NULL,
  `content` longtext DEFAULT NULL,
  `id_author` longtext DEFAULT NULL,
  `image` longtext DEFAULT NULL,
  `title` varchar(500) DEFAULT NULL,
  `link_rewrite` varchar(500) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `updated_by_id` int(11) unsigned DEFAULT NULL,
  `view_count` int(11) DEFAULT 0,
  `status` int(11) DEFAULT 1,
  PRIMARY KEY (`id_news`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,4,4,'<p>Test First News With TitleTest First News With TitleTest First News With TitleTest First News With Title.<br />Test First News With TitleTest First News With TitleTest First News With Title.<br /><br /><br />Test First News With Title.<br /><br />Test First News With Title.<br /><br />Test First News With Title</p>','1','vespa-notte-125br.jpg','Test First News With Title','test-first-news-with-title','2021-01-04 10:18:09','2021-01-04 10:18:09',NULL,NULL,38,1),(2,4,4,'<p>This is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakki.<br /><br />This is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakki..<br />This is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakki.</p>\r\n<p>This is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakkiThis is best chakki.</p>','1','activa-6g5e53815437235-1.jpg','This is best chakki','this-is-best-chakki','2021-01-04 10:46:43','2021-01-04 10:46:43',NULL,NULL,0,1),(3,4,4,'<p>Scribes of the time recorded the Vedic age as one of great progress, and we now have the Vaidik Age in which another scribe shows us great progress in journalism; where an international scoop goes unprinted and yet the scribe in question, Ved Pratap Vaidik, still makes headlines. His interview with terror mastermind Hafiz Saeed is still to see the light of day, but portions of it have been leaked by the ISI. Here are unofficial excerpts:</p>\r\n<p>&nbsp;</p>\r\n<p>Saeed: Welcome to my humble, charitable, philanthropic abode. I have been told you are a very important man.</p>\r\n<p>Vaidik: Yes, in our country, I am called a VIP. I&rsquo;m sure you have them here too.</p>\r\n<p>Saeed: All the VIPs we have are from your country &mdash; Dawood Ibrahim, Tiger Memon, Yasin Bhatkal&hellip;</p>\r\n<p>Vaidik: I am here to change your thinking and put you on a different track.</p>\r\n<p>Saeed: Is this Track II diplomacy?</p>\r\n<p>Vaidik: You can say that if you go by my track record. I am a famous journalist, which is why the Pakistan media is chasing me for interviews. I am also close to the powers that be, so there are two tracks.</p>\r\n<p>Saeed: You are interviewing me without a tape recorder or notebook, so which track are you on today? I am just doing philanthropy. Which reminds me, have some biryani&hellip;</p>\r\n<p>Vaidik: I am not here for biryani diplomacy. I stayed back to meet you and try and change your approach. Have you tried yoga? My mentor Baba Ramdev is as famous as me and he also acts on behalf of the powers that be, but yoga can do wonders for people who have strayed. I call it the Vaidik posture &mdash; you wear a red vest and put egg on your face. It makes you oblivious to criticism.</p>\r\n<p>Saeed: My mentors told me to answer your questions since it would give me a platform and change my image. What is your question?</p>\r\n<p>Vaidik: How many wives do you have? You see, if you are projected as a man with many wives, it sends a positive signal. Anyone who can keep three wives happy must have some done some good in his past life. In India, you are seen as a terror mastermind.</p>\r\n<p>Saeed: That is my past life when I was minding my masters. The Pakistan courts have cleared me, the ISI has backed me, the Pakistan government needs me. I am just a harmless social worker.</p>\r\n<p>Vaidik: And I am just a journalist. But your attitude to India is well known.</p>\r\n<p>Saeed: I embrace Indians, all those who come to me for help, from Kashmir, from Punjab. This is JuD, a training ground for people who need some purpose and direction in their lives. I point them in the right direction, across the LoC.</p>\r\n<p>Vaidik: We now have a strong leader who is not afraid to call a spade a spade, or have a heart to heart with your Prime Minister. He is a true diamond. In case he decides to visit Pakistan, I hope you will not create any problems.</p>\r\n<p>Saeed: I have no problems, as long as you invite me to India. So far, I have been sponsoring lots of people for visits to India, sometimes even by sea, but it would be nice to be officially invited to your country. You say you are close to Narendra Modi, maybe you can help.</p>\r\n<p>Vaidik: He may not be so happy with me after this.</p>\r\n<p>Saeed: Why is that?</p>\r\n<p>Vaidik: He has been attending a summit with world leaders. I am here with you, and I will have made more headlines than him.</p>','1','activa-6g5e53815437235-1.jpg','Flip side: The Vaidik Age','flip-side-the-vaidik-age','2021-01-23 10:11:54','2021-01-23 10:11:54',NULL,NULL,1,1);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `sub_title` varchar(500) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `link_rewrite` varchar(50) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by_id` varchar(50) DEFAULT NULL,
  `updated_by_id` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (4,'Vaidic Chakki','This is a vaidic chakki which follow vaidic process of make flour....','<p>This is a vaidic chakki which follow vaidic process of make flour. This is a vaidic chakki which follow vaidic process of make flour. This is a vaidic chakki which follow vaidic process of make flour... t</p>','atta-chakki-machine-1465434.jpg','vaidic-chakki','2020-09-13 08:16:46','2020-09-13 03:00:59',NULL,NULL,1),(5,'Mashale','Vaidic mashale is a best things for our nature','<p>Vaidic mashale is a best things for our nature.Vaidic mashale is a best things for our natureVaidic mashale is a best things for our natureVaidic mashale is a best things for our nature</p>','spice-mix-ayurveda-veganism-curry-powder-png-favpng-AFmze4SuBDB8tXCtn1J86VM82.jpg','mashale','2020-09-13 08:33:15','2020-09-13 03:03:15',NULL,NULL,1),(6,'Other Services','Other ServicesOther ServicesOther ServicesOther Services','<p>Other ServicesOther ServicesOther ServicesOther ServicesOther ServicesOther ServicesOther ServicesOther ServicesOther Services</p>','logo.jpg','other-services','2020-09-13 08:34:45','2020-09-13 03:04:45',NULL,NULL,1);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `id_review` int(11) NOT NULL AUTO_INCREMENT,
  `id_model` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `review` text DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `quality` tinyint(4) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by_id` varchar(50) DEFAULT NULL,
  `updated_by_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_review`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,3,4,'jhdsdf fdsufdsf dfuidf dfuidgd mgdfi gigd gdig dfgidf gdffg dfigf gdfgifdg dfgifdg dfgidf fgif gdfgi','asd fgfdgdfg','7727939593',1,5,'2020-10-18 10:17:53','2020-10-18 04:47:53',NULL,NULL),(2,3,5,'jhdsdf fdsufdsf dfuidf dfuidgd mgdfi gigd gdig dfgidf gdffg dfigf gdfgifdg dfgifdg dfgidf fgif gdfgi ','asd fgfdgdfg','7727939593',1,5,'2020-10-18 10:20:57','2020-10-18 04:50:57',NULL,NULL),(3,3,4,'jhdsdf fdsufdsf dfuidf dfuidgd mgdfi gigd gdig dfgidf gdffg dfigf gdfgifdg dfgifdg dfgidf fgif gdfgi ','asd fgfdgdfg','7727939593',1,5,'2020-10-18 10:21:27','2020-10-18 04:51:27',NULL,NULL),(4,3,4,'jhdsdf fdsufdsf dfuidf dfuidgd mgdfi gigd gdig dfgidf gdffg dfigf gdfgifdg dfgifdg dfgidf fgif gdfgi ','asd fgfdgdfg','7727939593',1,5,'2020-10-18 10:22:37','2020-10-18 04:52:37',NULL,NULL),(5,3,4,'jhdsdf fdsufdsf dfuidf dfuidgd mgdfi gigd gdig dfgidf gdffg dfigf gdfgifdg dfgifdg dfgidf fgif gdfgi ','asd fgfdgdfg','7727939593',1,5,'2020-10-18 10:23:12','2020-10-18 04:53:12',NULL,NULL),(6,3,4,'jhdsdf fdsufdsf dfuidf dfuidgd mgdfi gigd gdig dfgidf gdffg dfigf gdfgifdg dfgifdg dfgidf fgif gdfgi ','asd fgfdgdfg','7727939593',1,5,'2020-10-18 10:24:43','2020-10-18 04:54:43',NULL,NULL),(7,3,4,'jhdsdf fdsufdsf dfuidf dfuidgd mgdfi gigd gdig dfgidf gdffg dfigf gdfgifdg dfgifdg dfgidf fgif gdfgi ','asd fgfdgdfg','7727939593',1,5,'2020-10-18 10:25:53','2020-10-18 04:55:53',NULL,NULL),(8,3,4,'jhdsdf fdsufdsf dfuidf dfuidgd mgdfi gigd gdig dfgidf gdffg dfigf gdfgifdg dfgifdg dfgidf fgif gdfgi ','asd fgfdgdfg','7727939593',1,5,'2020-10-18 10:30:00','2020-10-18 05:00:00',NULL,NULL);
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specs`
--

DROP TABLE IF EXISTS `specs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specs` (
  `id_specs` int(11) NOT NULL AUTO_INCREMENT,
  `id_model` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `specs` text DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `launch_date` datetime DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by_id` varchar(50) DEFAULT NULL,
  `updated_by_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_specs`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specs`
--

LOCK TABLES `specs` WRITE;
/*!40000 ALTER TABLE `specs` DISABLE KEYS */;
INSERT INTO `specs` VALUES (1,8,5,'[{\"name\":\"motor\",\"value\":\"0.5 kmpl\"}]',1,NULL,NULL,'2021-01-24 16:59:45',NULL,NULL),(2,5,4,'[{\"name\":\"Wood\",\"value\":\"Sagwan\"},{\"name\":\"Chakki Size\",\"value\":\"14 inch\"},{\"name\":\"Motor Power\",\"value\":\"0.5 HP\"},{\"name\":\"heigth\",\"value\":\"3 ft\"},{\"name\":\"Iron Use\",\"value\":\"NO\"},{\"name\":\"Floor Quality\",\"value\":\"Nice\"},{\"name\":\"Floor Heat\",\"value\":\"Normal\"},{\"name\":\"Production Per Hour\",\"value\":\"2 Kg\"}]',1,NULL,NULL,'2021-01-24 17:05:50',NULL,NULL);
/*!40000 ALTER TABLE `specs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) NOT NULL COMMENT 'phone number',
  `otp` varchar(255) DEFAULT NULL COMMENT 'Random One Time Password generated',
  `otp_expire` int(11) DEFAULT NULL COMMENT 'OTP Expire time in unix timestamp format',
  `auth_key` varchar(255) NOT NULL COMMENT 'will be used by Yii 2 auth system',
  `created_on` int(11) DEFAULT NULL COMMENT 'Registration time in unix timestamp format',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES (1,'7727939593','',NULL,'i-6SElmrRRGWdeCehOR5ClM8F_UUzuPI',1590220238),(2,'9928704975','290560',1590304950,'krxCCuiEumohkcLhdtQ23mh_HVzuW2NX',1590229832),(3,'7063975683','935397',1590230759,'EPg53biG-KAD44IpHIqiqZdCHbbynvXc',1590229857),(4,'+17063975683','239519',1590231541,'KMsuliNX014_vlnbXOuyF422GeyiCsqL',1590230379),(5,'9929870013','164851',1590338573,'palzQu_aOx24H68HCeWh0N7-MaSPPXmA',1590320724);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `createdAt` int(11) DEFAULT NULL,
  `updatedAt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (1,'sandeep',NULL,NULL);
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic` (
  `id_topic` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `link_rewrite` varchar(500) NOT NULL,
  `side_menu_title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `id_category` int(4) NOT NULL,
  `status` tinyint(4) DEFAULT 0,
  `index_position` int(4) DEFAULT NULL,
  `created_by_id` varchar(50) DEFAULT NULL,
  `updated_by_id` varchar(50) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_topic`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic`
--

LOCK TABLES `topic` WRITE;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
INSERT INTO `topic` VALUES (2,'Test topic ?','test-topic-','Test Topic','<p>Test Topic&nbsp;<br /><br />Test Topic<br />Test Topic</p>\r\n<p><br />Test Topic</p>',2,1,NULL,NULL,NULL,'2020-06-21 08:46:31','2020-06-21 04:10:04'),(3,'What is Node.js?','what-is-nodejs','Introduction','<p>Node.js is a server-side platform built on Google Chrome\'s JavaScript Engine (V8 Engine). Node.js was developed by Ryan Dahl in 2009 and its latest version is v0.10.36. The definition of Node.js as supplied by its official documentation is as follows &minus;</p>\r\n<p>Node.js is a platform built on Chrome\'s JavaScript runtime for easily building fast and scalable network applications. Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient, perfect for data-intensive real-time applications that run across distributed devices.</p>\r\n<p>Node.js is an open source, cross-platform runtime environment for developing server-side and networking applications. Node.js applications are written in JavaScript, and can be run within the Node.js runtime on OS X, Microsoft Windows, and Linux.</p>\r\n<p>Node.js also provides a rich library of various JavaScript modules which simplifies the development of web applications using Node.js to a great extent.</p>\r\n<p>Node.js = Runtime Environment + JavaScript Library</p>',2,1,NULL,NULL,NULL,'2020-06-24 16:48:49','2020-06-24 11:18:49');
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `emailEnc` varchar(108) DEFAULT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `createtime` int(10) NOT NULL DEFAULT 0,
  `lastvisit` int(10) NOT NULL DEFAULT 0,
  `superuser` int(1) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `is_admin` int(1) DEFAULT 0,
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `updated_by_id` int(11) unsigned DEFAULT NULL,
  `source` enum('web','wap','app-and','app-ios','webform','pd','gaadi','zigwheels','vg') DEFAULT NULL,
  `and_dwn_sms` enum('0','1','2') DEFAULT '0',
  `is_sync` enum('0','1') NOT NULL DEFAULT '0',
  `is_subscribe` enum('0','1','2') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`),
  KEY `index_username` (`username`),
  KEY `idx_users_emailEnc` (`emailEnc`)
) ENGINE=InnoDB AUTO_INCREMENT=12734308 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (12734306,'Sandeep Jangir','TPwtAXQWqPW32pzI','sandeep.jangir@girnarsoft.com',NULL,'j3lcD8FTTMOviBGPb0MXR2JajoG6O20t',1591063701,0,0,1,1,NULL,NULL,NULL,'0','0','1'),(12734307,'shyamsunder jangid','-ftyWD9inRXrIQOc','shyamsunderjangid52@gmail.com',NULL,'ylo3dyu84ZlEkGaRXsUEpPSlGJ7OBN8Q',1591538699,0,0,1,1,NULL,NULL,NULL,'0','0','1');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id_videos` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `id_model` int(11) NOT NULL,
  `highlight` longtext DEFAULT NULL,
  `video_value` longtext DEFAULT NULL,
  `id_author` longtext DEFAULT NULL,
  `image` longtext DEFAULT NULL,
  `video_title` varchar(500) DEFAULT NULL,
  `link_rewrite` varchar(500) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `updated_by_id` int(11) unsigned DEFAULT NULL,
  `view_count` int(11) DEFAULT 0,
  `status` int(11) DEFAULT 1,
  `video_caption` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_videos`) USING BTREE,
  KEY `idx_videos_id_model` (`id_model`),
  KEY `idx_videos_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (1,4,4,'Frist Videos Frist Videos Frist Videos Frist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist VideosFrist Videos','funny-status-short-status-10-second-status.mp4','1','5fc619ec51158.png','Frist Videos','frist-videos','2020-12-19 05:59:23','2020-12-19 06:13:11',NULL,NULL,0,1,NULL),(2,5,3,'Mrich Paudar Mrich Paudar Mrich Paudar Mrich Paudar Mrich Paudar Mrich Paudar Mrich Paudar Mrich Paudar Mrich Paudar Mrich Paudar ','funny-status-short-status-10-second-status.mp4','1','5f649e4169b21.png','Mrich Paudar  Mrich Paudar  Mrich Paudar ','mrich-paudar-mrich-paudar-mrich-paudar-','2020-12-22 13:14:13','2020-12-22 13:14:13',NULL,NULL,0,1,NULL);
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-25 10:28:58
