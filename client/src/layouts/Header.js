import React from 'react';
import { Navbar, Nav, NavDropdown} from 'react-bootstrap';
import { MdMenu} from 'react-icons/md'
import Search from '../widgets/Search';
import configs from '../configs/configs';

function Header() {
  return (
    <header id="header" className="fixed-top">
      <Navbar collapseOnSelect expand="lg" >

        <div className="container">
          <Navbar.Brand href="/">{configs.productName}</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav">
            <MdMenu/>
          </Navbar.Toggle>
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <NavDropdown 
                title="Products" 
                id="collasible-nav-dropdown"
                > 
                <NavDropdown.Item href="/vaidic-chakki.html">Vaidic Chakki</NavDropdown.Item>
                <NavDropdown.Item href="/mashale.html">Masala</NavDropdown.Item>
                <NavDropdown.Item href="/other-services.html">Other Service</NavDropdown.Item>
              </NavDropdown>
              <Nav.Link href="/videos">Videos</Nav.Link>
              <Nav.Link href="/news">News</Nav.Link>
              <Nav.Link href="/community">Community</Nav.Link>
              <Nav.Link href="/about-us">About Us</Nav.Link>
              <Nav.Link href="/contact-us">Contact Us</Nav.Link>
            </Nav>
            <Search containerCls="mr-sm-2"/>
          </Navbar.Collapse>
        </div>
        </Navbar>
    </header>
  );
}

export default Header;
