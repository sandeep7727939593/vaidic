import React from 'react';
import  './../static/css/cta.css';

const defaultProps = {
	style :{ display: "none" }
};

class LeadFrom extends React.Component {

	//Client Side
    constructor(props) {
        super(props);
        this.state = window.__INITIAL_STATE__ ||  defaultProps;
    }
    componentDidMount() {
    	let url = window.location.href;
		if(url.includes("#leadFrom")) {
			this.setState({style :{ display: "block" }})
		}
    }
   
    
    render() {

    	let handleClose = (e) => {
			this.setState({style :{ display: "none" }})
        }


    	return (
		    <div id="myModal" className={`cta-popup`} style={this.state.style}>
			   <div className="popup-content">
			    <span className="close" onClick={handleClose}>&times;</span>
			    <h1>Sorry</h1>
			    <p>Technical Problem</p>
			   </div>
			</div>
		);
    }
}
LeadFrom.defaultProps = defaultProps;
export default LeadFrom;
