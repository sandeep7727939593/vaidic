import React from 'react';
import Api from './../utils/Api';
import Breadcrumb from './../widgets/Breadcrumb';
import ModelCard from './../widgets/ModelCard';
import ReviewCard from './../widgets/ReviewCard';
import Specification from './../widgets/Specification';
import Benefit from './../widgets/Benefit';
import globals from './../utils/globals';
import FaqAccordion from './../widgets/FaqAccordion';
import MetaTag from './../utils/MetaTag';
import ShimmerEffect from './../widgets/ShimmerEffect';

const defaultProps = {
    breadcrumb : [],
    modelData : {
        title: ''
    },
    review : {},
    specsList : {},
    faqList : {},
    benefitList : {},
    seoData : {},
    isLoad : false
};
class ModelPage extends React.Component {
	//Client Side
    constructor(props) {
        super(props);
        this.state = window.__INITIAL_STATE__ ||  defaultProps;
    }

    componentDidMount() {
        let self = this;
        self.setState(defaultProps);
        let params = this.props.match.params;
        let q = {
            modelslug : params.model,
        };
        Api.getData('modelPage', q).then((res => {
            if(!globals.isEmptyObject(res)) {
                if(!res.data.breadcrumb || res.data === {}) {
                    return;
                }
                let o = {
                    breadcrumb : res.data.breadcrumb ? res.data.breadcrumb : this.defaultProps.breadcrumb,
                    modelData : res.data.modelData ? res.data.modelData : this.defaultProps.modelData,
                    review : res.data.review ? res.data.review : this.defaultProps.review,
                    specsList : res.data.specsList ? res.data.specsList : this.defaultProps.specsList,
                    faqList : res.data.faqList ? res.data.faqList : this.defaultProps.faqList,
                    benefitList : res.data.benefitList ? res.data.benefitList : this.defaultProps.benefitList,
                    seoData : res.data && res.data.seoData ? res.data.seoData : defaultProps.seoData,
                    isLoad : true
                };
                self.setState(o);
            }
        })).catch((err) => {
            console.log(err);
        });
    }

    render() {
        if(!this.state.isLoad) {
            return (
                <main className="model-page">
                    <ShimmerEffect type="page"/>
                </main>
            )
        }
        if(!this.state.modelData.title) {
            return null;
        }

        return (
		    <main className="model-page">
                 <Breadcrumb breadcrumb={this.state.breadcrumb}/>
                 <ModelCard modelData={this.state.modelData}/>
                 <Specification data={this.state.specsList} />
                 <ReviewCard review={this.state.review}/>
                 <FaqAccordion faqList={this.state.faqList}/>
                 <Benefit data={this.state.benefitList}/>
                <MetaTag data={this.state.seoData} />
		    </main>
		);
    }
}
ModelPage.defaultProps = defaultProps;
export default ModelPage;
