import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import React from 'react';

const GoogleMapWidget = withScriptjs(withGoogleMap((props) => {
  	
  	return (
			<GoogleMap
				defaultZoom={16}
				defaultCenter={{ lat: 27.248964, lng: 75.450972 }}
				>
				{props.isMarkerShown && <Marker position={{ lat: 27.248964, lng: 75.450972 }} />}
			</GoogleMap>
	);
}))
export default GoogleMapWidget;