/**
 * News.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
tableName : 'news',
    attributes: {
        id: { 
           type: 'number',  
           columnName : 'id_news',
           // required: true,
           unique:true,
           autoIncrement:true,
        },
        id_product: { 
            type: 'number', 
            required: true 
        },
        id_model: { 
            type: 'number', 
            required: true 
        },
        id_author: { 
            type: 'string', 
            required: true 
        },
        title: { 
            type: 'string', 
            required: true 
        },
        content: { 
            type: 'string', 
            required: true 
        },
        video_title: { 
            type: 'string'
        },
        view_count: { 
            type: 'number'
        },
        add_date: { 
            type: 'ref', 
            required: true 
        },
        update_date: { 
            type: 'ref', 
            required: true 
        },
        status: { 
            type: 'number', 
            required: true 
        },
    },
    getList : async (params = []) => {
        let query = News.selectQuery('news', params);
        return await News.getDatastore().sendNativeQuery(query, []);
    },

    updateReord : async (condition, value) => {
        return await News.updateOne(condition).set(value);
    }

};

