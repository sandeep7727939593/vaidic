/**
 * HomeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */



module.exports = {
  
	getModelPage: async (req, res) => {

		res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		
		let modelslug = req.query.modelslug;

		let p = {
			filters : [
				'status=1',
				`link_rewrite='${modelslug}'`
			]
		};
		var model = await Model.getList(p);
		if(sails.hooks.common.isEmptyObject(model.rows)) {
			res.json({});
		}
		model = model.rows[0];

		let modelData = {
	        title : model.model_name,
	        
	        desc : sails.hooks.common.getDesc(model.description),
	        image : sails.hooks.common.imageUrl(model.cover_image, 'model/'),
	        images : [
	            sails.hooks.common.imageUrl(model.cover_image, 'model/'),
	            sails.hooks.common.imageUrl(model.cover_image, 'model/'),
	            sails.hooks.common.imageUrl(model.cover_image, 'model/'),
	            sails.hooks.common.imageUrl(model.cover_image, 'model/'),
	            sails.hooks.common.imageUrl(model.cover_image, 'model/'),
	        ],
	        price : model.base_price,
	        voteText : '<strong>91%</strong> of buyers enjoyed this product! <strong>(87 votes)</strong>',

	    };
	    let pr = {
			filters : [
				`id_model='${model.id_model}'`,
				`status='1'`,
			],
			orderBy : 'rating DESC'
		};
		var review = await Review.getList(pr);
		pr.fields = [
			'count(*) as reviewGroup',
			'rating',
		];
		pr.groupBy = 'rating';
		var reviewGroup = await Review.getList(pr);

		review = review.rows;
		var reviewList = [];
		let subTitle = '';
		let reviews = {};
		if(!sails.hooks.common.isEmptyObject(review)) {
			let totalReviewCount = review.length;
			let reviewSum = 0;
			for (i in review) {
				reviewSum = reviewSum + review[i]['rating'];
				if(i > 4) {
					continue;
				}
				d = new Date(review[i]['add_date']);
				reviewList.push({
				    "rating": review[i]['rating'],
				    "review":review[i]['review'],
				    "name": review[i]['name'],
				    "add_date": d.toLocaleString('en-US', {month: 'short', day: '2-digit', year: 'numeric'}),
				});
			}
			let rating = Math.round((reviewSum/totalReviewCount) * 10) / 10;
			modelData.rating = rating;
	        modelData.ratingText = `${totalReviewCount} reviews`;
	        modelData.ratingTitle = `${totalReviewCount} reviews`;
	        let reviewItem = [];
	        reviewGroup = reviewGroup.rows;
			for (var a = 0; a < 5; a++) {
				reviewItem.push({
					rating:reviewGroup[a] && reviewGroup[a]['rating'] ? reviewGroup[a]['rating'] : 5-a,
					per : (reviewGroup[a] && reviewGroup[a]['reviewGroup'] ? ((reviewGroup[a]['reviewGroup']*100)/totalReviewCount) : 0) + '%',
					review : reviewGroup[a] && reviewGroup[a]['reviewGroup'] ? reviewGroup[a]['reviewGroup'] : 0,
				});
			}
	        reviews = {
	        	totalReviewCount : totalReviewCount,
	        	rating : rating,
		    	title : `Top ${model.model_name} Reviews`,
		    	items : reviewList,
		    	reviewGroup : reviewItem,
		    	subTitle : `${modelData.rating} average based on ${totalReviewCount} reviews`,
		    };
	    }
	    let params = {
			filters : [
				`id_product='${model.id_product}'`,
			]
		};
		var product = await Product.getList(params);
		product = product.rows[0];

		// GET Specification

		let paramsSpec = {
			filters : [
				`id_model='${model.id_model}'`,
				`status=1`,
			]
		};
		var specs = await Specs.getList(paramsSpec);
		specs = specs.rows;
		var specsList = {};
		if(!sails.hooks.common.isEmptyObject(specs)) {
			specs = specs[0];
			specsList.items = JSON.parse(specs['specs'])
			specsList.title = 'Specification And Features';
		}

		// Get Faq
		let paramsFaq = {
			filters : [
				'status=1',
				`id_model='${model.id_model}'`,
			],
			orderBy : "id_faq desc",
			limit : 5,
		};
		var faq = await Faq.getList(paramsFaq);
		var faq = faq.rows;
		let faqList = [];
		faq.map((item, index) => {
			faqList.push({
				question : item.question,
                answer : item.answer,
			});
		});

		// GET Specification

		let paramsBenefit = {
			filters : [
				`id_model='${model.id_model}'`,
				`status=1`,
			]
		};
		var benefit = await Benefit.getList(paramsBenefit);
		benefit = benefit.rows;
		var benefitList = {};
		if(!sails.hooks.common.isEmptyObject(benefit[0])) {
			benefit = benefit[0];
			benefitList.items = JSON.parse(benefit['value'])
			benefitList.title = 'Benefit';
		}

		let data = {
			modelData : modelData,
			breadcrumb : [
		        {
		            text : 'Home',
		            title : 'Home',
		            url : '/',
		            active : false,
		        },
		        {
		            text : product.title,
		            title : product.title,
		            url : sails.hooks.common.productUrl(product.link_rewrite),
		            active : false,
		        },
		        {
		            text : model.model_name,
		            title : model.model_name,
		            url : sails.hooks.common.modelUrl(product.link_rewrite, model.link_rewrite),
		            active : true,
		        },
		    ],
		    review : reviews,
		    specsList : specsList,
		    faqList : {
				title : 'Frequently Asked Questions',
				items : faqList,
			},
			benefitList : benefitList,
			seoData : {
				title : model.model_name,
				description : model.model_name + ' @' + sails.config.domainName,
				index : '',
			},
		};
		res.json(data);
	}
};

