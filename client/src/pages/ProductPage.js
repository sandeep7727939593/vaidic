import React from 'react';
// import Model from '../widgets/Model';
import Api from './../utils/Api';
import Breadcrumb from './../widgets/Breadcrumb';
import PageContent from './../widgets/PageContent';
import ProductCard from './../widgets/ProductCard';
import globals from './../utils/globals';
import FaqAccordion from './../widgets/FaqAccordion';
import Benefit from './../widgets/Benefit';
import MetaTag from './../utils/MetaTag';
import ShimmerEffect from './../widgets/ShimmerEffect';

const defaultProps = {
    breadcrumb : [],
    pageContent : {},
    items : [],
    colcls:'',
    btncls:'',
    benefitList : {},
    faqList : {},
    seoData: {},
    isLoad : false
};

class ProductPage extends React.Component {
	//Client Side
    constructor(props) {
        super(props);
        this.state = window.__INITIAL_STATE__ ||  defaultProps;
    }

    componentDidMount() {
        this.setState(defaultProps);

        let params = this.props.match.params;
        let q = {
            productslug : params.product,
        };
        Api.getData('productPage', q).then((res => {
            if(!globals.isEmptyObject(res)) {
                if(res.data && res.data.status === 301) {
                    window.location.href = res.data.url;
                    return;
                }
                if(!res.data.items || res.data === {}) {
                    return;
                }
                let o = {   
                    breadcrumb : res.data.breadcrumb ? res.data.breadcrumb : this.defaultProps.breadcrumb,
                    pageContent : res.data.pageContent ? res.data.pageContent : this.defaultProps.pageContent,
                    items : res.data.items ? res.data.items : this.defaultProps.items,
                    faqList : res.data.faqList ? res.data.faqList : this.defaultProps.faqList,
                    benefitList : res.data.benefitList ? res.data.benefitList : this.defaultProps.benefitList,
                    seoData : res.data && res.data.seoData ? res.data.seoData : defaultProps.seoData,
                     isLoad : true
                };
                this.setState(o);
            }
        })).catch((err) => {
            console.log(err);
        });
    }
    
    

    render() {

        if(!this.state.isLoad) {
            return (
                <main>
                    <ShimmerEffect type="page" />
                </main>
            )
        }
        if(!this.state.items) {
            return null;
        }

        let cards = (items) => {
            return(
                <>
                    {items.map((item, index) => <ProductCard key={index} item={item} colcls={'col-lg-4 col-md-6'} btncls={'btn-success mt-3'} />)}
                </>
            );
        }
    	return (
            <main>
                    <Breadcrumb breadcrumb={this.state.breadcrumb} />
                    <PageContent pageContent={this.state.pageContent} />
                    {this.state.items && <section className="pb-4">
                       <div className="container">
                        <div className="row">
                            {cards(this.state.items)}
                        </div>
                        </div>
                    </section>}
                    <FaqAccordion faqList={this.state.faqList}/>
                    <Benefit data={this.state.benefitList}/>
                    <MetaTag data={this.state.seoData} />
		    </main>
		);
    }
}
ProductPage.defaultProps = defaultProps;
export default ProductPage;
