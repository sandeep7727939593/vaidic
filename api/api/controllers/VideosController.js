/**
 * VideosController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
	videoPage : async (req, res) => {
		let pageSize = req.query.pageSize ? req.query.pageSize : 5;
		let pageNo = req.query.pageNo ? req.query.pageNo : 1;
		
		let params = {
			fields : [
				'videos.*',
				'author.name as author_name'
			],
			filters : [
				'status=1',
			],
			join : [
				'LEFT JOIN author ON videos.id_author=author.id_author'
			]
		};
		var videosCount = await Videos.getList(params);
		params.limit = pageSize;
		params.page = pageNo;
		var videos = await Videos.getList(params);

		var response = {};
		if(!sails.hooks.common.isEmptyObject(videos.rows)) {
			videos = videos.rows;
			videosCount = videosCount.rows.length;
			let videoList = [];
			videos.map((item, index) => {
				var obj = {
					title : item.video_title,
					highlight : sails.hooks.common.getShortDesc(item.highlight) + '...',
					videoUrl : sails.hooks.common.videoUrl(item.video_value, 'videos/'),
					img : sails.hooks.common.imageUrl(item.image, 'videos-image/'),
					viewCount:item.view_count,
					url : sails.hooks.common.videoDetailUrl(item.link_rewrite, false),
					date : sails.hooks.common.dateFormatter(item.add_date),
					author_name : item.author_name,
				};
				videoList.push(obj);
			});
			response.videoList = videoList;
			response.url = sails.hooks.common.videoLandingUrl(false);
			response.pagination = {
				totalCount : videosCount,
				pageNo : pageNo,
				pageSize : pageSize,
				pageCount : Math.ceil(videosCount/pageSize)
			};
			response.pageContent = {
				title : 'Videos List',
				content : 'Video marketing strategy is not what it used to be a few years ago. Today, it has gone to become a lot more serious, and much more effective, especially for businesses that have already managed to build an audience.'
			};
			response.seoData = {
				title : 'Video Page',
				description : 'Video Page @' + sails.config.domainName,
				index : '',
			};	
		}
		res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		res.json(response);
	},

	videoDetailPage : async (req, res) => {
		let slug = req.query.slug;
		let params = {
			fields : [
				'videos.*',
				'author.name as author_name'
			],
			filters : [
				'status=1',
				`videos.link_rewrite='${slug}'`
			],
			join : [
				'LEFT JOIN author ON videos.id_author=author.id_author'
			]
		};
		var videos = await Videos.getList(params);
		var response = {};
		if(!sails.hooks.common.isEmptyObject(videos.rows)) {
			videos = videos.rows[0];
			videos.videoUrl = sails.hooks.common.videoUrl(videos.video_value, 'videos/');
			response = {
				breadcrumb : [
					{
			            text : 'Home',
			            title : 'Home',
			            url : '/',
			            active : false,
			        },
			        {
			            text : 'Videos',
			            title : 'Videos',
			            url : sails.hooks.common.videoLandingUrl(true),
			            active : false,
			        },
			        {
			            text : videos.video_title,
			            title : videos.video_title,
			            url : '',
			            active : true,
			        },
				],
				videos : videos,
				pageContent : {
					title : videos.video_title,
					content : videos.highlight
				},
				seoData : {
					title : videos.video_title,
					description : videos.highlight,
					index : '',
				}	
			};
		}
		res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		res.json(response);
	}
};

