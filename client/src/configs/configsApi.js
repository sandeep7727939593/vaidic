module.exports = {
	'homePage' : 'api/getHomePage',
	'productPage' : 'api/getProductPage',
	'modelPage' : 'api/getModelPage',
	'postWriteReview' : 'api/postWriteReview',
	'videoPage' : 'api/videoPage',
	'videoDetailPage' : 'api/videoDetailPage',
	'newsPage' : 'api/newsPage',
	'newsDetailPage' : 'api/newsDetailPage',
	'search' : 'api/search',
	'community' : 'api/getCommunity',
};