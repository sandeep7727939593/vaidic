const userAgent = navigator.userAgent.toLowerCase();

const detect = {
	isMobile : () => {
		return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(userAgent)
	},
	isDesktop : () => {
		return /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i.test(userAgent);
	}
};
export default detect;