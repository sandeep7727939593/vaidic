/**
 * HomeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */



module.exports = {
  
	getHomePage: async (req, res) => {

		let params = {
			filters : [
				'status=1',
			]
		};
		var product = await Product.getList(params);
		var product = product.rows;
		let items = [];
		product.map((item, index) => {
			items.push({
				text : item.title,
                title : item.title,
                url : sails.hooks.common.productUrl(item.link_rewrite),
                logo : sails.hooks.common.imageUrl(item.image, 'product/'),
                desc : sails.hooks.common.getShortDesc(item.description, 200),
			});
		});
		let banner = [
			{	
				'title' : 'Banner 1',
				'img' : sails.config.cdnUrl + 'banner/banner2.jpg',
				'active' : true, 
			},
			{	
				'title' : 'Banner 2',
				'img' : sails.config.cdnUrl + 'banner/banner2.jpg',
				'active' : false, 
			},
			{	
				'title' : 'Banner 3',
				'img' : sails.config.cdnUrl + 'banner/banner2.jpg',
				'active' : false, 
			},
		];
		let paramsFaq = {
			filters : [
				'status=1',
				'id_product IS NULL',
				'id_model IS NULL',
			],
			orderBy : "id_faq desc",
			limit : 5,
		};
		var faq = await Faq.getList(paramsFaq);
		var faq = faq.rows;
		let faqList = [];
		faq.map((item, index) => {
			faqList.push({
				question : item.question,
                answer : item.answer,
			});
		});

		// GET Specification

		let paramsBenefit = {
			filters : [
				'id_product IS NULL',
				'id_model IS NULL',
				`status=1`,
			]
		};
		var benefit = await Benefit.getList(paramsBenefit);
		benefit = benefit.rows;
		var benefitList = {};
		if(!sails.hooks.common.isEmptyObject(benefit[0])) {
			benefit = benefit[0];
			benefitList.items = JSON.parse(benefit['value'])
			benefitList.title = 'Benefit';
		}


		let data = {
			product: {
				title : 'Our Products',
				items : items
			},
			faqList : {
				title : 'Frequently Asked Questions',
				items : faqList,
			},
			banner : banner,
			benefitList : benefitList,
			seoData : {
				title : sails.config.domainName,
				description : 'Our Products @' + sails.config.domainName,
				index : '',
			},
		};
		res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		res.json(data);
	}
};

