import React from 'react';
import { Helmet } from "react-helmet";

const MetaTag = (props) => {
      let data = props.data;

	if(typeof data === undefined && typeof data.title === undefined) return;

	return (
	    <Helmet>
            <meta charSet="utf-8" />
            {data.title ? <title>{data.title}</title> : ''}
            {data.canonicalUrl ? <link rel="canonical" href={data.canonicalUrl} /> : ''}
            {data.ogImage ? <title><meta property="og:image" content={data.ogImage}/></title> : ''}
            {data.description ? <meta name="twitter:description" 
            property="og:description" 
            itemprop="description" 
            content={data.description} /> : ''}
            {data.index && data.index !== '' ? <meta name="robots" content={data.index} /> : ''}
        </Helmet>
	);
}
export default MetaTag;
