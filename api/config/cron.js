const formatDate = () => {
  console.log('formatDate')
  var d = new Date(),
  month = '' + (d.getMonth() + 1),
  day = '' + d.getDate(),
  year = d.getFullYear();
  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;
  return [year, month, day].join('-');
};

module.exports.cron = {
  myFirstJob: {
    schedule: '15 11 * * *',
    onTick: function () {
    	 var fs = require('fs');
        date =  formatDate();
    	  var sitemap = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"><url><loc>http://www.chakiwale.in/api/main.xml</loc><lastmod>' + date + '</lastmod></url></urlset>';
        fs.writeFile("./assets/api/sitemap.xml", sitemap, function(err){
           if(err) {
               return console.log('Error', err);
           }
           console.log("The file was saved!");
           return;  
       });
    }
  },
  secondJob: {
    schedule: '15 11 * * *',
    onTick: function () {
       var fs = require('fs');
        date =  formatDate();
        var sitemap = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        sitemap += '<url><loc>http://www.chakiwale.in</loc><lastmod>' + date + '</lastmod><changefreq>daily</changefreq></url>';
        sitemap += '<url><loc>http://www.chakiwale.in/vaidic-chakki.html</loc><lastmod>' + date + '</lastmod><changefreq>daily</changefreq></url>';
        sitemap += '<url><loc>http://www.chakiwale.in/mashale.html</loc><lastmod>' + date + '</lastmod><changefreq>daily</changefreq></url>';
        sitemap += '<url><loc>http://www.chakiwale.in/vaidic-chakki/hand-chakki.html</loc><lastmod>' + date + '</lastmod><changefreq>daily</changefreq></url>';
        sitemap += '<url><loc>http://www.chakiwale.in/vaidic-chakki/wood-chakki.html</loc><lastmod>' + date + '</lastmod><changefreq>daily</changefreq></url>';
        sitemap += '<url><loc>http://www.chakiwale.in/videos</loc><lastmod>' + date + '</lastmod><changefreq>daily</changefreq></url>';
        sitemap += '<url><loc>http://www.chakiwale.in/news</loc><lastmod>' + date + '</lastmod><changefreq>daily</changefreq></url>';
        sitemap += '<url><loc>http://www.chakiwale.in/community</loc><lastmod>' + date + '</lastmod><changefreq>daily</changefreq></url>';
        sitemap += '</urlset>';
        fs.writeFile("./assets/api/main.xml", sitemap, function(err){
           if(err) {
               return console.log('Error', err);
           }
           console.log("Seocnd file was saved!");
           return;  
       });
    }
  }
};