 /*eslint no-unused-expressions: 0*/

import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './Routes';
import Header from './layouts/Header';
import './static/scss/vaidic.scss';
import * as serviceWorker from './serviceWorker';


ReactDOM.hydrate(
  <React.StrictMode>
    <Header />
  	<Routes />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
