/**
 * Community.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
    tableName : 'community',
    attributes: {
        id: { 
           type: 'number',  
           columnName : 'id_faq',
           // required: true,
           unique:true,
           autoIncrement:true,
        },
        type: { 
            type: 'string', 
            required: true 
        },
        description: { 
            type: 'string', 
            required: true 
        },
        status: { 
            type: 'number', 
            required: true 
        },
    },
    getList : async (params = []) => {
        let query = Community.selectQuery('community', params);
        return await Community.getDatastore().sendNativeQuery(query, []);
    }

};

