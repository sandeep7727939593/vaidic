import React from 'react';
import { browserHistory } from 'react-router'; //import Redirect 

export default class Click extends React.Component{

    constructor(props)
    {
        super(props);
        this.state = {
            click : false
        }
        this.handleOnClick = this.handleOnClick.bind();
    }

    handleOnClick = () => {
        this.setState({click: true});
    }

    render() {
        if (this.state.click) {
            browserHistory.push(this.props.url);
            return true;
        }

        return (
            <button className={this.props.cls} onClick={this.handleOnClick} type="button">{this.props.title}</button>
        );
    }
}