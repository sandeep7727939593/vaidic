import React from 'react';
import './../static/scss/_card.scss';
import {Card, CardDeck} from 'react-bootstrap';
// import Carousel from 'react-bootstrap/Carousel'
const Product = (props) => {
  let item = props.product;

  return (
    
      <CardDeck>
        <a href={item.url} className="link-block">
          <Card className="shadow radius-top-10">
            <div className="img-block bg-overlay-gradient-04" data-autofit="true" style={{ backgroundImage: `url(${item.logo})`, height: "250px" }}><Card.Img variant="top" src={item.logo} /></div>
            <Card.Body>
              <Card.Title>{item.title}</Card.Title>
              <Card.Text className="short-text-line3">
                {item.desc}
              </Card.Text>
            </Card.Body>
          </Card>
        </a> 
      </CardDeck>);

}
export default Product;
