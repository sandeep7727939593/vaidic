import axios from 'axios';
import configsApi from './../configs/configsApi';
import querystring from 'querystring';
import configs from './../configs/configs';

export default {
    getData: (key, QueryData = {}) => axios({
        'method':'GET',
        'url': configs.apiUrl + configsApi[key],
        'headers': {
            'content-type':'application/javascript',
        },
        'params': QueryData,
    }).then(res => {
        if(res.status === 200) {
            return res;
        }

    }).catch(err => {
        console.log('API Error' + err);
    }),

    postData: function(key, data, cb){
        var xhr = new XMLHttpRequest();
        xhr.open("POST", configs.apiUrl + configsApi[key], true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send(querystring.stringify(data));
        xhr.onload = function() {
            //console.log(xhr)
            //alert(xhr.responseText);
            try{
                var parsedResponse = JSON.parse(xhr.responseText);
                cb(parsedResponse);
            }catch(e) {
                cb(undefined);
            }
        };

         xhr.onerror = function(){
            cb(undefined);
        };
    }
}