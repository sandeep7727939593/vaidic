/**
 * Home.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
module.exports = {
    tableName : 'model',
    attributes: {
        id: { 
           type: 'number',  
           columnName : 'id_model',
           required: true,
        },
        id_product: { 
            type: 'number', 
            required: true 
        },
        model_name: { 
            type: 'string', 
            required: true 
        },
        short_name: { 
            type: 'string', 
            required: true 
        },
        description: { 
            type: 'string'
        },
        link_rewrite: { 
            type: 'string', 
            required: true 
        },
        cover_image: { 
            type: 'string', 
            required: true 
        },
        add_date: { 
            type: 'string', 
            required: true 
        },
        update_date: { 
            type: 'string', 
            required: true 
        },
        status: { 
            type: 'number', 
            required: true 
        },
        base_price: { 
            type: 'number', 
            required: true 
        },
        popularity: { 
            type: 'number', 
            required: true 
        },
        alternative_link_rewrite: { 
            type: 'string', 
            required: true 
        },
        alternative_link_type: { 
            type: 'string', 
            required: true 
        },
    },
    getList : async (params = []) => {
        let query = Model.selectQuery('model', params);
        return await Model.getDatastore().sendNativeQuery(query, []);
    }
};

