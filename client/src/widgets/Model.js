import React from 'react'

const Model = (props) => {
 const {handleClose} = props;
 return (
  <div id="myModal" className={`cta-popup`} style={{ display: "none" }}>
   <div className="popup-content">
    <span className="close" onClick={handleClose}>&times;</span>
    <h1>Sorry</h1>
    <p>Technical Problem</p>
   </div>
  </div>
 )
}

export default Model
