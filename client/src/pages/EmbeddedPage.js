import React from 'react';
import './../static/css/embedded.css';
import { FacebookProvider, EmbeddedPost, Comments} from 'react-facebook';
// import InstagramEmbed from 'react-instagram-embed';
import { TwitterTweetEmbed } from 'react-twitter-embed';
import configs from './../configs/configs';
import MetaTag from './../utils/MetaTag';
import globals from './../utils/globals';
import Api from './../utils/Api';
import Pagination from './../widgets/Pagination';
import ShimmerEffect from './../widgets/ShimmerEffect';
import queryString from 'query-string';

const defaultProps = {
    items : [],
    seoData : {},
    title : '',
    pagination : {},
    url : '',
    isLoad : false,
};
class Embedded extends React.Component {
    //Client Side
    constructor(props) {
        super(props);
        this.state = window.__INITIAL_STATE__ ||  defaultProps;
    }

    componentDidMount() {
        this.setState(defaultProps);
        let params = queryString.parse(this.props.location.search)
        let q = {
            pageSize : 2,
            pageNo : params.pageno ? params.pageno : 1,
        };
        Api.getData('community', q).then((res => {
            if(!globals.isEmptyObject(res)) {
                let o = {   
                    items : res.data.items ? res.data.items : defaultProps.items,
                    seoData : res.data.seoData ? res.data.seoData : defaultProps.seoData,
                    title : res.data.title ? res.data.title : defaultProps.title,
                    pagination : res.data.pagination ? res.data.pagination : defaultProps.pagination,
                    url : res.data.url ? res.data.url : defaultProps.url,
                    isLoad : true
                };
                this.setState(o);    
            }
        })).catch((err) => {
            console.log(err);
        });
    }
    render() {
        if(!this.state.isLoad) {
            return (
                <main className="home">
                    <ShimmerEffect type="card"/>
                </main>
            )
        }
        if(this.state.items === [] || typeof this.state.items  === undefined) {
            return null;
        }
        let items = this.state.items;
        let container = (data) => {
            for (var i = 0; i < data.length; i++) {
                // switch(data[i].type) {
                //     case 'facebook' : 
                // }
            }
        }
            
    	return (
		    <div className="embedded">
                
                <h1>{this.state.title}</h1>
                {container(items)}
                {
                    items.map((item, index) => 
                    <div>
                      {item.type === 'facebook' ? 
                        <FacebookProvider appId={configs.facebookAppId}>
                            <EmbeddedPost href={item.description}/>
                            <Comments href={item.description}/>
                        </FacebookProvider>            
                        : (item.type === 'twitter' ? 
                            <TwitterTweetEmbed
                                tweetId={item.description}/> : '')     
                      }  
                    </div>)
                }
                
                <hr />
                <Pagination url={this.state.url} data={this.state.pagination}/>
                <MetaTag data={this.state.seoData} />
		    </div>
		);
    }
}
export default Embedded;
