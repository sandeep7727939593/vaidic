import React from 'react';
import Api from './../utils/Api';
import detect from './../utils/detect';
import configs from '../configs/configs';

class Search extends React.Component {
  constructor() {
    super();
    this.state = {
        slug: '',
        name:'',
        suggestion : '',
        items : [],
    };
    this.typingHandle = this.typingHandle.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleSecrchCB = this.handleSecrchCB.bind(this);
  }
  typingHandle(e) {
    let value = e.target.value;
    let suggestion = '';
    let items = [];
    if(value.length > 0) {
        let q = {
            text : value
        };
        Api.getData('search', q).then((res) => {
            var data = res.data ? res.data : [];
            for (var i = 0; i < data.length; i++) {
                suggestion += `<li value="${data[i].slug}" >${data[i].name}</li>`;
                items.push({name : data[i].name, slug:data[i].slug});
            }
            this.setState({suggestion  : suggestion, items : items});
        }); 
    } else {
        this.setState({suggestion  : 'We Have Not Found Any Result'});
    }
    
    this.setState({name  : value});
  }

  handleClick(e) {
    let  element = e.target;
    let url = element.getAttribute("value");
    console.log(url)
    if(url) {
        window.location.href = configs.siteUrl + url;
    }
  }

  handleBlur() {
    let self = this;
    setTimeout(function() {
        document.getElementById("suggestion").value= '';
        self.setState({suggestion  : ''});
    }, 1000);
  }
  handleSecrchCB(e) {
    if(this.state.items && this.state.items.length > 0) {
        let data = this.state.items[0];
        window.location.href = data.slug;
    }
  }

  render() {
    if(detect.isMobile()) return null;
    return (
      <div className={`search d-flex ${this.props.containerCls}`}>
            <input 
                id="suggestion"
                placeholder="Search your product"
                onKeyUp={((e) => this.typingHandle(e))}
                onBlur={((e) => this.handleBlur(e))}
                className="form-control"
            />
            <button type="button" onClick={(e) => this.handleSecrchCB(e)} className="btn btn-outline-success">Search</button><br />
            {this.state.suggestion !== '' && <ul className="search-list" onClick={(e) => this.handleClick(e)} dangerouslySetInnerHTML={{__html : this.state.suggestion}} />}
        </div>
    );
  }
}

export default Search;