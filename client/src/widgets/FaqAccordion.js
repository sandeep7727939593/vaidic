import React from 'react';
import { Accordion, AccordionItem, AccordionItemHeading, AccordionItemButton, AccordionItemPanel} from 'react-accessible-accordion';
import './../static/scss/_accordion.scss';
 
const FaqAccordion = (props) => {
        let data = props.faqList;
        let items = data.items;
        if(typeof items === 'undefined' || items.length <= 0) {
            return null;
        }

        return (
            <div>{<Accordion allowZeroExpanded preExpanded={[0]} className="section">
                <h2 className="title">{data.title}</h2>
                <div className="wrap">  
                {items.map((item, index) => 
                    <AccordionItem key={index} uuid={index}>
                        <AccordionItemHeading>
                            <AccordionItemButton>
                                {item.question}
                            </AccordionItemButton>
                        </AccordionItemHeading>
                        <AccordionItemPanel>
                            <p>{item.answer}</p>
                        </AccordionItemPanel>
                    </AccordionItem>
                )}
                </div>
            </Accordion>}
                
            </div>
        );
}

export default FaqAccordion;
