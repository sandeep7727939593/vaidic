/**
 * common hook
 *
 * @description :: A hook definition.  Extends Sails by adding shadow routes, implicit actions, and/or initialization logic.
 * @docs        :: https://sailsjs.com/docs/concepts/extending-sails/hooks
 */

module.exports = function defineCommonHook(sails) {

  return {

    /**
     * Runs when this Sails app loads/lifts.
     */
    initialize: async function() {

      sails.log.info('Initializing custom hook (`common`)');

    },

    productUrl : (link, prefix = true) =>  {
    	let url = (prefix ? sails.config.urlPath : '') + link + '.html';
    	return url;
    },

    modelUrl : (productlink, modellink, prefix = true) =>  {
    	let url = (prefix ? sails.config.urlPath : '') + productlink + '/' + modellink  + '.html';
    	return url;
    },

    writeReviewUrl : (modelId, prefix = true) =>  {
    	let url = (prefix ? sails.config.urlPath : '') + 'write-review/' + modelId;
    	return url;
    },

    writeReviewDetail : (modelId, reviewId, prefix = true) =>  {
    	let url = (prefix ? sails.config.urlPath : '') + 'write-review/' + modelId + '/' + reviewId;
    	return url;
    },

    videoDetailUrl : (link, prefix = true) =>  {
    	let url = (prefix ? sails.config.urlPath : '') + 'videos/' + link;
    	return url;
    },

    newsDetailUrl : (link, prefix = true) =>  {
    	let url = (prefix ? sails.config.urlPath : '') + 'news/' + link;
    	return url;
    },

    videoLandingUrl : (prefix = true) =>  {
    	let url = (prefix ? sails.config.urlPath : '') + 'videos';
    	return url;
    },

    newsLandingUrl : (prefix = true) =>  {
    	let url = (prefix ? sails.config.urlPath : '') + 'news';
    	return url;
    },

    communityUrl : (prefix = true) =>  {
    	let url = (prefix ? sails.config.urlPath : '') + 'community';
    	return url;
    },

    imageUrl : (url, type) =>  {
    	return sails.config.cdnUrl + type + url;
    },

    videoUrl : (url, type) =>  {
    	return sails.config.mediaUrl + type + url;
    },

    getShortDesc : (content, length = 80) => {
	    // strip tags to avoid breaking any html
	    if(content == '') return '';
	    content = content.toString();
	    content = content.replace(/<\/?[^>]+(>|$)/g, "");
	    content =content.replace("\n", "");
	    content =content.replace("\t", "");
	    content =content.replace("\r", "");
	    content = content.replace("&nbsp;", "");
	    if (content.length > length) {
	        let trimmedString = content.substr(0, length);
			content = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
			content += '...'; 
	    }
	    content.trim();
	    return content;
	},

	getDesc : (content) => {
	    // strip tags to avoid breaking any html
	    if(content == '') return '';
	    content = content.toString();
	    content = content.replace(/<\/?[^>]+(>|$)/g, "");
	    content =content.replace("\n", "");
	    content =content.replace("\t", "");
	    content =content.replace("\r", "");
	    content = content.replace("&nbsp;", "");
	    return content;
	},

	isEmptyObject : (obj) => {
	    try {
	        if (typeof obj === 'undefined' || obj == null || Object.keys(obj).length == 0) {
	            if(typeof obj == 'Array' && obj.length > 0){
	                return false;
	            }
	        return true;
	        }
	        return false;    
	    } catch (e) {
	        return true;
	    }
	    
	},

	dateFormatter : (date) => {
		var options = {year: 'numeric', month: 'long', day: 'numeric' };
		var date  = new Date(date);
		return date.toLocaleDateString("en-US", options);
	},
	redirectUrl : (url) => {
		return {
			url : url,
			status : 301
		};
	}
  };

};
