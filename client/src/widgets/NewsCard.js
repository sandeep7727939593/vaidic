import React from 'react';
import { Card, CardDeck } from 'react-bootstrap'

const NewsCard = (props) => {
	if(typeof props.items === 'undefined') return null;
  let item = props.items;
  const { colcls } = props;
	return (
	    <div className={colcls}>
      <CardDeck>
       
          <Card className="shadow">
            <div className="img-block bg-overlay-gradient-04" data-autofit="true" style={{ backgroundImage: `url(${item.logo})`, height: "250px" }}><Card.Img variant="top" src={item.img} alt={item.title}/></div>
            <Card.Body>
            <Card.Title> <a href={item.url} className="link-block">{item.title}</a></Card.Title>
              <Card.Text className="short-text-line3">
                {item.content}
              </Card.Text>
            <Card.Text className="short-text-line3">
              <small className="text-muted">On {item.date} | {item.viewCount} Views</small>
            </Card.Text>
            </Card.Body>
          <Card.Footer>
            <small className="text-muted">By {item.author_name}</small>
           
          </Card.Footer>

          </Card>
      </CardDeck>
      </div>
	);
}
export default NewsCard;
