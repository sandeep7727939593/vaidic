import React from 'react';
import detect from './../utils/detect';
import { Card, CardDeck } from 'react-bootstrap'

const VideoCard = (props) => {
	if(typeof props.items === 'undefined') return null;
	let items = props.items;
  let width = detect.isMobile() ? "225px" : "500px";
  const { colcls} = props;
  
	return (
    <>
      { items.map((item, index) => <div className={colcls} key={index}>
        <CardDeck>
          <Card className="shadow">
            <div className="img-block bg-overlay-gradient-04" data-autofit="true" style={{ backgroundImage: `url(${item.logo})`, height: "250px" }}><a href={item.url}><Card.Img variant="top" src={item.img} alt={item.title} width={width} /></a></div>
            <Card.Body>
              <Card.Title> <a href={item.url} className="link-block">{item.title}</a></Card.Title>
              <Card.Text className="short-text-line3">
                {item.highlight}
              </Card.Text>
              <Card.Text className="short-text-line3">
                <small className="text-muted">On {item.date} | {item.viewCount} Views</small>
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">By {item.author_name}</small>

            </Card.Footer>

          </Card>
        </CardDeck>
      </div> )}
    </>
	);
}
export default VideoCard;
