import React from 'react';
import Rating from './../widgets/Rating';
import Cta from './../widgets/Cta';
import './../static/css/model-card.css';

const ModelCard = (props) => {
  let modelData = props.modelData;
  let images = (images) => {
    let h = [];
    images.map((item, index) => 
      h.push(<div key={index} className={`tab-pane ${index === 0 ? 'active' : ''}`} id={`pic-${index}`}><img src={item} alt="Images"/></div>)
    )
    return h;
  }
  let imagesBottom = (images) => {
    let h = [];
    images.map((item, index) => 
      h.push(<li key={index} className={index === 0 ? 'active' : ''}><a href="/#" data-target={`#pic-${index}`} data-toggle="tab" ><img src={item} alt="Images" /></a></li>)
    )
    return h;
  }
	return (
    <section className="pt-4 pb-4">
        <div className="container">
          <div className="container-fliud">
            <div className="wrapper row">
              <div className="preview col-md-6">

                <div className="preview-pic tab-content">
                  {images(modelData.images)}
                </div>
                <ul className="preview-thumbnail nav nav-tabs">
                  {imagesBottom(modelData.images)}
                </ul>

              </div>
              <div className="details col-md-6">
              <h1 className="title">{modelData.title}</h1>
                <div className="rating">
                  <Rating name="half-rating" defaultValue={2.5} precision={0.5} rating={modelData.rating} />
                  <span className="review-no" title={modelData.ratingTitle}>{modelData.ratingText}</span>
                </div>
                <p className="product-description">{modelData.desc}</p>
                <h4 className="price">price: <span>{modelData.price}</span></h4>
                <p className="vote" dangerouslySetInnerHTML={{ __html: modelData.voteText }} />
                <div className="d-inline-block">
                    <Cta btncls="btn-success" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
	);
}
export default ModelCard;
