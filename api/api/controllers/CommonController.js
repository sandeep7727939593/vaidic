/**
 * FaqController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
	search : async function(req, res) {
		res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

		var data = [];
		let text = req.query.text;
		if(sails.hooks.common.isEmptyObject(text)) {
			res.send({});
		}
		let params = {
			filters : [
				`title LIKE '%${text}%' OR link_rewrite LIKE'%${text}%'`,
				'status=1'
			],
			fields : [
				"title",
				"link_rewrite"
			]
		};
		var product = await Product.getList(params);

		if(!sails.hooks.common.isEmptyObject(product.rows)) {
			let productList = product.rows;
			productList.map((item, index) => {
				data.push({
					name : item.title,
					slug : sails.hooks.common.productUrl(item.link_rewrite, false)
				})
			})
		}

		let paramsModel = {
			filters : [
				`model_name LIKE '%${text}%' OR model.link_rewrite LIKE'%${text}%' OR short_name LIKE'%${text}%'`,
				'model.status=1'
			],
			fields : [
				"model_name",
				"model.link_rewrite as model_link_rewrite",
				"product.link_rewrite as brand_link_rewrite",
			],
			join : [
				'LEFT JOIN product ON model.id_product=product.id_product'
			]
		};
		var model = await Model.getList(paramsModel);

		if(!sails.hooks.common.isEmptyObject(model.rows)) {
			let modelList = model.rows;
			modelList.map((item, index) => {
				data.push({
					name : item.model_name,
					slug : sails.hooks.common.modelUrl(item.brand_link_rewrite, item.model_link_rewrite, false)
				})
			})
		}
	
		res.send(data);
	}
};

