import React from 'react';
import Api from './../utils/Api';
import MetaTag from './../utils/MetaTag';
// import Breadcrumb from './../widgets/Breadcrumb';
import globals from './../utils/globals';
import ShimmerEffect from './../widgets/ShimmerEffect';

const defaultProps = {
    breadcrumb : [],
    modelData : {
        title: ''
    },
    review : {},
    seoData : {},
    isLoad : false
};
class NewsDetailPage extends React.Component {
	//Client Side
    constructor(props) {
        super(props);
        this.state = window.__INITIAL_STATE__ ||  defaultProps;
    }

    componentDidMount() {
        let self = this;
        self.setState(defaultProps);
        let params = this.props.match.params;
        let q = {
            slug : params.newsUrl,
        };
        Api.getData('newsDetailPage', q).then((res => {
            if(!globals.isEmptyObject(res)) {
                let o = {
                    news : res.data.news ? res.data.news : {},
                    pageContent : res.data.pageContent ? res.data.pageContent : {},
                    seoData : res.data.seoData ? res.data.seoData : {},
                    breadcrumb : res.data.breadcrumb ? res.data.breadcrumb : [],
                    isLoad : true
                };
                self.setState(o);
            }
        })).catch((err) => {
            console.log(err);
        });
    }
    
    render() {
         if(!this.state.isLoad) {
            return (
                <main className="home">
                    <ShimmerEffect type="page"/>
                </main>
            )
        }
        if(!this.state.news) {
            return null;
        }

        return (

		    <main className="news-detail-page">
                <div className="container">
                    <div className="row">
                        <div className="col-6">
                            <img className="img-fluid rounded float-start" src={this.state.news.img} alt="" />
                        </div>
                        <div className="col-6">
                            <section>
                                <h1>{this.state.news.title}</h1>
                                <p>{`Publish On ${this.state.news.addDate} By ${this.state.news.author_name} | ${this.state.news.view_count ? this.state.news.view_count + ' Views' : ''}`}</p>
                                <article className="news-desc" dangerouslySetInnerHTML={{ __html: this.state.news.content }} />
                                <MetaTag data={this.state.seoData} />
                            </section>
                        </div>
                    </div>
                </div>       
		    </main>
		);
    }
}
NewsDetailPage.defaultProps = defaultProps;
export default NewsDetailPage;
