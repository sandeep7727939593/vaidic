import React from 'react';
import Api from './../utils/Api';
import HomeBanner from './../widgets/HomeBanner';
import Product from './../widgets/Product';
// import Carousel from './../widgets/Carousel';
import FaqAccordion from './../widgets/FaqAccordion';
import AboutMe from './../widgets/AboutMe';
import globals from './../utils/globals';
import Benefit from './../widgets/Benefit';
import MetaTag from './../utils/MetaTag';
import ShimmerEffect from './../widgets/ShimmerEffect';
import Carousel from 'react-bootstrap/Carousel'

const defaultProps = {
	banner : [],
    product : {
        title : 'Our Products',
        items : []
    },
    faqList : [],
    showAcIcon:false,
    benefitList : {},
    seoData : {},
    isLoad : false
};

class IndexPage extends React.Component {

	//Client Side
    constructor(props) {
        super(props);
        this.state = window.__INITIAL_STATE__ ||  defaultProps;
    }

    componentDidMount() {
        this.setState(defaultProps);

    	Api.getData('homePage').then((res => {
            if(!globals.isEmptyObject(res)) {
                let o = {   
                    banner : res.data.banner ? res.data.banner : defaultProps.banner,
                    product : res.data.product ? res.data.product : defaultProps.product,
                    faqList : res.data.faqList ? res.data.faqList : defaultProps.faqList,
                    benefitList : res.data.benefitList ? res.data.benefitList : defaultProps.benefitList,
                    seoData : res.data && res.data.seoData ? res.data.seoData : defaultProps.seoData,
                    isLoad : true
                };
                this.setState(o);    
            }
    	})).catch((err) => {
            console.log(err);
        });
    }
    
    render() {
        let stateData = this.state;

        if(!this.state.isLoad) {
            return (
                <main className="home">
                    <ShimmerEffect type="page"/>
                </main>
            )
        }

        if(typeof stateData.product.items[0] === 'undefined') return null;

    	return (
		    <main className="home">
		      <HomeBanner banner={stateData.banner} />
                <section className="section">
                  <Carousel>
                        {stateData.product && stateData.product.items && stateData.product.items.map((item, index) => 
                            <Carousel.Item><Product product={item} key={index}/></Carousel.Item>)
                        }
                    </Carousel>
                </section>
                <AboutMe />
                <FaqAccordion faqList={this.state.faqList}/>
                <Benefit data={this.state.benefitList}/>
                <MetaTag data={this.state.seoData} />
		    </main>
		);
    }
}
IndexPage.defaultProps = defaultProps;
export default IndexPage;
