import React from 'react';
import { FaFacebookF, FaTwitter, FaLinkedin, FaYoutube} from 'react-icons/fa';
import LazyLoad from 'react-lazyload';
import configs from '../configs/configs';

class Footer extends React.Component  {

    render() {

        let date = new Date();
        let year = date.getFullYear();
        return (
        <LazyLoad scroll>
            <footer id="footer" className="clearfix">
                <div className="container d-md-flex py-4">
                    <div className="me-md-auto text-center text-md-left">
                        <div className="copyright">
                            © Copyright <strong><span>{year} {configs.productName}</span></strong>.All Rights Reserved. 
                        </div>
                        <div className="credits">     
                            Proudly created by Sandeep Kumar Jangir
                        </div>
                    </div>
                    <div className="social-links text-center text-md-right pt-3 pt-md-0">
                        <a href="https://twitter.com" className="twitter"><FaTwitter/></a>
                        <a href="https://facebook.com" className="facebook"><FaFacebookF/></a>
                        <a href="https://linkedin.com" className="linkedin"><FaLinkedin/></a>
                        <a href="https://youtube.com" className="youtube"><FaYoutube/></a>
                    </div>
                </div>
            </footer>
        </LazyLoad>)
    }
}

export default Footer;