/**
 * Benefit.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
    tableName : 'benefit',
    attributes: {
        id: { 
           type: 'number',  
           columnName : 'id_faq',
           // required: true,
           unique:true,
           autoIncrement:true,
        },
        id_product: { 
            type: 'number', 
            required: true 
        },
        id_model: { 
            type: 'number', 
            required: true 
        },
        value: { 
            type: 'string', 
            required: true 
        },
        add_date: { 
            type: 'ref', 
            required: true 
        },
        update_date: { 
            type: 'ref', 
            required: true 
        },
        status: { 
            type: 'number', 
            required: true 
        },
    },
    getList : async (params = []) => {
        let query = Benefit.selectQuery('benefit', params);
        return await Benefit.getDatastore().sendNativeQuery(query, []);
    }

};


